##flask server 
### 1.采用：flask框架， 实时数据通信采用flask-socketio 
### 2.采用flask-socketio进行实时数据推送
### 3.当前版主要功能
+ 1 采用两个queue进行数据缓存，字节源数据queue以及解析后数据queue
+ 2 解析后数据源为全局queue，接下来会进行修改，分离
+ 3 具备文件解析回放功能
+ 4 数据解析部分API功能可以用于实时数据接收处理共用
+ 5 分离全局数据queue以及独立回访文件queue，独立文件回放采用单独Thread进行操作，利用request.sid建立room号，进行面向独立client的数据通信
+ 6 频谱数据以及完成前台合并，emit 数据完成修改分离
+ 7 解析对应xml文件，得到相应的地图相关信息
+ 8 增加mysql数据库，增加xml批量解析入库功能以及sql导出xml功能（译码数据）,修改了写xml文件时中文乱码以及美化输出格式问题。xml文件中增加了新的zc元素
    + 采用sql模块中 deconding_xml_array 作为导出xml文件的管道
+ 9 启用multiprocessing Process进行译码数据接收解析入库，批量导入采用即接收即入库
+ 10 采用 tcp client 的方式对硬件设备进行连接，4路数据，采用4个端口
+ 11 spectrum 数据回放采用 vue-socketio，利用 callback 函数进行列表成功/异常数据返回。回放数据采用 $socket.id 作为 room id
+ 12 采用 blueprint 对项目业务结构进行划分，auth用于管理用户信息；coding用于管理交互指令信息；setting用于front-end对back-end进行配置信息设置，例如存储文件时，分割文件的大小。
    > 计划：译码数据以及接调数据都采用独立进程进行处理
    
    > *notice*: *socketio event 必须由gevent进程进行emit，subprocess无法进行socket emit 因为subprocess没有建立socket*

> 采用 room 进行独立面向 client 的数据通信
+ 13 文件存储操作采用独立进程，进程间变量采用 `multiprocessing.Manager().dict()` 进行变量共享。
    > 注意：Manger() dict() 对深层数据的更新问题。
+ 14 跨域情况`Access-Control-Allow-Origin`采用client的`HTTP_REFERER`
```python
async_mode = None
socketio = SocketIO(app, async = async_mode)
socketio.emit('my event', data, room=request.sid)
```
+ 15 增加Flask-SQLAlchemy 采用ORM模型对某些数据进行数据库操作
    > + 关于文件保存的配置内容信息（譬如，保存文件时文件的分割大小）  
    > + 注意update函数只能操作BaseQuery类型


#### 3.1地图数据
+ 地图数据来源与xml文件
+ 数据结构为xml解析对应的object对象


### 4.修改内容
##### 20200320-WriteXml入口参数有两个缩减为一个，取消xml—fold的参数暴露。完成xml批量入库以及导出前后端功能。

##### 20200321-upload 功能，多文件vue upload组件是按照单个文件独立进行post请求。server端适应此功能采用接收一个文件就立即入库一个文件。可以进行优化，后续。

##### 20200331-xml 解析适配新修订的xml文件格式，增加zcxx元素。修改数据库表结构

##### 20200417-xml 新修订的xml文件解析入库结束，批量导出功能正常。spectrum数据表增加`index`列(creat_time)。

##### 20200427 增加 tcp client

##### 20200428 完成对 spectrum 实时数据处理修改，对 decoding 文件入库一处type->types 处进行校正。对实时数据 spectrum 以及 decoding 发送显示进行了测试。 并对采样算法进行了修正。

##### 20200513 完成增加关口站，设备位置 别名设置。存储cfg文件至profile中。

##### 20200516 完成 spectrum 数据回放功能（前后端）,增加下载以及删除文件操作功能。

##### 20200519 完成文件存储操作api功能，修改了对应的 tcp 接收等。

##### 20200616 tcp recv 默认是 blociking 状态

##### 20200703 增加ORM模型，并对文件结构进行进一步划分，增加exts config 以及model模块

##### 20200709 对文件ORM模型及交互式数据修改操作进行修正

##### 20200716 对uploadXml指令进行移植，从app转移至coding/view文件中，并对指令发送进行写数据库操作，关联发送后日志查询显示

#### 20200723 对接口文件增加了译码处理接口，处理算法可以按照实际需求进行修改接口函数内容