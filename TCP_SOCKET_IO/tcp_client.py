#!/usr/bin/env python3
# -*- encoding:utf-8 -*-
# Author:caoy
# @Time:  14:13
import socket
#
# 创建接口
# 发起连接
# 创建接口参数同socket server相同
# 发起连接的函数为socket.connect(ip,port)


class TCPClient(object):

    def __init__(self, addr: str, port: int, buffer_size: int = 32768):
        self.server_addr = addr
        self.server_port = port
        self.client = None
        self.recv_buffer_size = buffer_size

    def init_tcp_client(self):
        failed_count = 0
        while True:
            try:
                self.creat_socket()
                self.creat_connect()
                self.client.setblocking(False)
                break
            except socket.error:
                print("<fail to connect to server %d times>" % failed_count)
                if failed_count == 100:
                    return

    def creat_socket(self):
        self.client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    def creat_connect(self):
        try:
            self.client.connect((self.server_addr, self.server_port))
        except socket.error as e:
            print('<Tcp client creat connect error!-%s>' % e)

    def translate_data(self, recv_queue, save_queue, send_queue,
                       parse_flag: bool = True, save_flag: bool = True):
        """
        : use to translate data from server and client
        :param recv_queue: the queue used to receive data from server
        :param save_queue: the queue used to write data file
        :param send_queue: the queue used to send data to server
        :param parse_flag: the flag used to identify if parse the received data
        :param save_flag: the flag used to identify if save in queue
        :return:
        """
        self.init_tcp_client()
        while True:
            try:
                # send data
                if send_queue is not None:
                    if not send_queue.empty():  # not empty
                        try:
                            self.client.send(send_queue.get(block=False))
                        except Exception:
                            pass
                # recv data
                try:
                    recv = self.client.recv(self.recv_buffer_size)
                except BlockingIOError:
                    continue
                # print(len(recvs))
                if parse_flag:
                    recv_queue.put(recv)

                if save_flag:
                    save_queue.put(recv, block=False)
                del recv
            except socket.error as e:
                print('<tcp client receive data error!-%s>' % e)
                self.init_tcp_client()
            except Exception as e:
                print('<tcp client receive and put queue error!-%s>' % e)


if __name__ == "__main__":
    client = TCPClient('192.168.1.198', 9000)
    client.translate_data([], [])