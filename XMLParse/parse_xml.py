#!/usr/bin/env python3
# -*- encoding:utf-8 -*-
# Author:caoy
# @Time:  10:23

import os
import xml.etree.ElementTree as ET


class ParseXML(object):
    def __init__(self, full_path):
        self.__check_path(full_path)
        # self.__tree = self.__creat_tree()
        # self._root = self._getRoot()
        # self.xml_info = self.__creat_xml_info()     # parse the xml and creat the xml information object

    def __check_file(self, tag='data'):
        """check the xml file is satisfied the standard"""
        return str(tag) == self._root.tag

    def __check_path(self, path):
        try:
            if isinstance(path, str) and path.lower().endswith('.xml'):    # single xml file
                self.path = path
                self.__tree = self.__creat_tree()
                self._root = self._getRoot()
                self.xml_info = self.__creat_xml_info()  # parse the xml and creat the xml information object
            elif isinstance(path, list):   # not only one file
                self.paths = path
            else:
                self.xml_info = None
        except Exception as e:
            print('<init parse xml class error!{}>'.format(e))

    def __creat_tree(self):
        """create element tree object"""
        # check if is xml file
        if os.path.isfile(self.path):
            parser = ET.XMLParser(encoding="utf-8")
            try:
                return ET.parse(self.path, parser=parser)
            except Exception as e:
                print("xml parsed exception!# {}".format(e))
        else:
            print("{} is not a file!".format(self.path))

    def _getRoot(self):
        """get the xml's root element"""
        return self.__tree.getroot()

    def _getSubRoot(self, root, tag: str):
        """
        : get the root's sub root element
        :param root:  the root element
        :param tag: sub root's tag
        :return:  the all sub's roots, the length of the sub's roots
        """
        sub_roots = root.findall(str(tag))
        return sub_roots, len(sub_roots)

    def __getText(self, element):
        """get the element's text value"""
        return element.text

    def __getAttrb(self, element):
        """get the element's attribute"""
        return element.attrib

    def __creat_xml_info(self):
        """ creat the single xml structure object(dict).Data structure comes from 57"""
        sub_root, num = self._getSubRoot(self._root, "系统解析")
        tags_list = []    # the '系统解析''s elements's tag
        info = {}
        for sub_root_item in sub_root:
            for item in sub_root_item:
                tags_list.append(item.tag)
            list(set(tags_list)).sort(key=tags_list.index)
            for tag in tags_list:                     # 网络号，关口站，卫星，点波束，上行基准频率kHz
                elements = sub_root_item.findall(tag)
                if len(elements) == 1:
                    # 只有找到一个 'xml.etree.ElementTree.Element'
                    if self.__getAttrb(elements[0]):
                        info[tag] = self.__getAttrb(elements[0])
                    else:
                        info[tag] = self.__getText(elements[0])   # 网络号, 上行基准频率KHz
                else:
                    # 包含多个  'xml.etree.ElementTree.Element'
                    temp = {}
                    for index, element in enumerate(elements):
                        if self.__getAttrb(element):
                            temp[tag+str(index+1).zfill(len(str(len(elements))))] = self.__getAttrb(element)
                    info[tag] = temp

        sub_root, num = self._getSubRoot(self._root, "侦控信息")
        sub_root_item = sub_root[0]
        for item in sub_root_item:      # 卫星编号 接收频率 接收频率 设备位置 处理时间
            tag = item.tag
            elements = sub_root_item.findall(tag)        # 侦控信息 对应sub element 均为单各element
            # 只有找到一个 'xml.etree.ElementTree.Element'
            if self.__getAttrb(elements[0]):
                info[tag] = self.__getAttrb(elements[0])
            else:
                info[tag] = self.__getText(elements[0])  # 网络号, 上行基准频率KHz

        return info

    def parse_xml_into_array(self):
        """
        :param xml_fold_path: the xmls fold path
        :param des: the des array of xml's object. It is a queue For caching the data
        :return:
        """
        # if os.path.isdir(xml_fold_path):
        #     xmls = os.listdir(xml_fold_path)
        #     for xml in xmls:
        #         if xml.lower().endswith('.xml'):
        #             des.append(self.__creat_xml_info())
        des = []
        for xml in self.paths:
            if os.path.isfile(xml):
                self.path = xml
                self.__tree = self.__creat_tree()
                self._root = self._getRoot()
                des.append(self.__creat_xml_info())

        return des


if __name__ == "__main__":
    # file = r'C:\Users\Lenovo\Pictures\57\分析应用软件参考文档，已脱密\GSC_DDC2100k_signalling（1）.xml'
    file = r'F:\docx\57所\XML格式\5F1_01_19707000_20200325100509_0001.xml'
    file1 = r'C:\Users\Lenovo\Pictures\57\分析应用软件参考文档，已脱密\verson2\insert_test1.xml'
    file2 = r'C:\Users\Lenovo\Pictures\57\分析应用软件参考文档，已脱密\verson2\insert_test2.xml'
    file3 = r'C:\Users\Lenovo\Pictures\57\分析应用软件参考文档，已脱密\verson2\insert_test3.xml'
    # file = r''
    files = [file1, file2, file3]
    # xml = ParseXML(file).xml_info
    xml = ParseXML(files).parse_xml_into_array()
    pass