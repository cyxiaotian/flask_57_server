#!/usr/bin/env python3
# -*- encoding:utf-8 -*-
# Author:caoy
# @Time:  9:11

# import time
import os
import zipfile
from commom import generater_file_name
import xml.etree.ElementTree as ET
from xml.dom import minidom





# {'上行基准频率kHz': '29507000',
#  '关口站': {'纬度': '36.75°S', '经度': '174.69°E'},
#   '卫星编号': '5F1',
# 	'接收频率': '19707000','
# 	'设备位置': {经度: "124.35°E" '纬度': "36.50°N"},
# 	'处理时间':'20200325100509','
#  '卫星': {'卫星1': {'信号频率': '19707000', '经度': '179.6°E'},
#         '卫星2': {'信号频率': '0', '经度': '31.5°E'},
#         '卫星3': {'信号频率': '19707000', '经度': '62.6°E'},
#         '卫星4': {'信号频率': '19708220', '经度': '117.5°E'},
#         '卫星5': {'信号频率': '19707000', '经度': '55.0°W'},
#         '卫星6': {'信号频率': '0', '经度': '0.7°W'}},
#  '点波束': {'点波束1': {'中心纬度': '11.40°S',
#                   '中心经度': '124.35°E',
#                   '前向载波速率kBd': '29090',
#                   '前向载波频率MHz': '19820',
#                   '点波束号': '1',
#                   '直径km': '1300'},
#                 ....},
# '网络号': '64'}

class WriteXML(object):
    def __init__(self, data: list):   # data: [{},{}...]
        self.root = ET.Element(str('data'))            # 创建xml root element
        self.data = data                # the xmd data need to creat xml file
        self.__xml_fold_path = r'./static/xml_file/download'
        self.__zip_fold_path = os.path.join(r'./static', 'zip_file')
        # self.zip_fold_path = r'./static/'
        self.xml_file_name = ''    # 写入的xml文件名

    def __writ_element_text(self, element: ET.Element, text: str):
        ''''''
        element.text = str(text)

    def __creat_subelement(self, parent_element: ET.Element,  tag_name: str, attrib={}):
        '''
        :  This function creates an element instance, and appends it to an existing element.
        :param parent_element:
        :param tag_name:
        :param attrib:
        :return: the element's instance
        '''
        return ET.SubElement(parent_element, str(tag_name), attrib)

    def write_xml_file(self):
        if isinstance(self.data, list):
            _ = list(map(self.__creat_xml_structue, self.data))
            # self.__write_to_file(self.root)   # original print
            self.__saveXML(self.root)           # pretty print

    def __creat_xml_structue(self, item: dict):
        """parse single dict to xml structure"""

        SAT_ID = '卫星编号'
        RECV_FREQUENCE = '接收频率'
        DEVICE_LOCATION = '设备位置'
        FRAME_TIME = '处理时间'
        UP_REF_REFERENCE = '上行频率基准kHz'
        GATWAY_STATION = '关口站'
        SAT = '卫星'
        DBS = '点波束'
        NETWORK_NUM = '网络号'
        SUB_ROOT_XT = '系统解析'
        SUB_ROOT_ZC = '侦控信息'

        sub_root_zx = self.__creat_subelement(self.root, SUB_ROOT_ZC)   # 侦察信息
        sub_root_xt = self.__creat_subelement(self.root, SUB_ROOT_XT)  # 系统解析

        if isinstance(item, dict):
            # write zc information
            sat_id = self.__creat_subelement(sub_root_zx, SAT_ID)
            self.__writ_element_text(sat_id, item[SAT_ID])

            recv_frequence = self.__creat_subelement(sub_root_zx, RECV_FREQUENCE)
            self.__writ_element_text(recv_frequence, item[RECV_FREQUENCE])

            device_location = self.__creat_subelement(sub_root_zx, DEVICE_LOCATION, item[DEVICE_LOCATION])

            frame_time = self.__creat_subelement(sub_root_zx, FRAME_TIME)
            self.__writ_element_text(frame_time, item[FRAME_TIME])

            # write xt information
            up_ref_refeence = self.__creat_subelement(sub_root_xt, UP_REF_REFERENCE)
            self.__writ_element_text(up_ref_refeence, item[UP_REF_REFERENCE])

            network_num = self.__creat_subelement(sub_root_xt, NETWORK_NUM)
            self.__writ_element_text(network_num, item[NETWORK_NUM])

            gatway_station = self.__creat_subelement(sub_root_xt, GATWAY_STATION, item[GATWAY_STATION])

            for e in item[SAT]:
                _ = self.__creat_subelement(sub_root_xt, SAT, item[SAT][e])

            for e in item[DBS]:
                _ = self.__creat_subelement(sub_root_xt, DBS, item[DBS][e])

        return self.root

    def __write_to_file(self, root: ET.Element):
        """writ to the file(xml)"""
        file = os.path.join(self.__xml_fold_path, generater_file_name()+'.xml')
        tree = ET.ElementTree(root)
        tree.write(file, xml_declaration=True, encoding="utf-8", method="xml")

    def __saveXML(self, root: ET.Element, indent="\t", newl="\n", encoding="utf-8"):
        """with beautiful format"""
        filename = os.path.join(self.__xml_fold_path, generater_file_name() + '.xml')
        self.xml_file_name = os.path.basename(filename)
        text = ET.tostring(root, encoding='utf-8', method='xml')
        dom = minidom.parseString(text)
        with open(filename, 'w', encoding=encoding) as fp_xml:          # encoding 防止乱码
            dom.writexml(fp_xml, "", indent, newl, encoding=encoding)

        try:
            self.__zip_file()
        except Exception as e:
            print('<zip xml file has error!{}>'.format(e))

    def __zip_file(self):
        """zip the xml file"""
        # zip_fold = os.path.join(os.path.dirname(self.__xml_fold_path), 'zip_file')
        zip = zipfile.ZipFile(os.path.join(self.__zip_fold_path, self.xml_file_name.split('.')[0]+'.zip'), 'w', zipfile.ZIP_DEFLATED)
        zip.write(os.path.join(self.__xml_fold_path, self.xml_file_name), self.xml_file_name)
        zip.close()



if __name__ == '__main__':
    from parse_xml import ParseXML
    array = []
    # file = r'C:\Users\Lenovo\Pictures\57\分析应用软件参考文档，已脱密\GSC_DDC2100k_signalling（1）.xml'
    # file = r'D:\python_project\flask_57\flask_server\static\xml_file\20200319_144313.xml'
    file = r'F:\docx\57所\XML格式\5F1_01_19707000_20200325100509_0001.xml'

    xml = ParseXML(file).xml_info
    for i in range(2):
        array.append(xml)
    w = WriteXML(array)
    w.write_xml_file()
    pass