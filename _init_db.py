


from exts import db, creat_app
from models import *


def init_db(app):
    try:
        db.create_all(app=app)
    except Exception as e:
        print("init db has exception!-{}".format(e))
    else:
        print("init db success!")


if __name__ == '__main__':
    app = creat_app()
    app.app_context().push()

    db.create_all(app=creat_app())
    # with app.app_context():
    #     # init_db(app=creat_app())
    #     db.create_all(app=creat_app())

