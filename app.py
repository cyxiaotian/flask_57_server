#!/usr/bin/env python3
# -*- encoding:utf-8 -*-
# Author:caoy
# @Time:  11:30

# from gevent import monkey
# monkey.patch_all()

import os
import time
# import random
import json
from copy import deepcopy
from urllib.parse import urljoin
from queue import Queue as T_Queue
from multiprocessing import Process, Pipe, Manager
from multiprocessing import Queue as M_Queue
# from engineio.async_drivers import gevent
# from engineio.async_drivers import threading
from flask import Flask, jsonify, request, copy_current_request_context, render_template, session, url_for, redirect
from werkzeug.utils import secure_filename
from flask_socketio import SocketIO, emit, disconnect, join_room, leave_room
from corsSupport import response_wrapper
from interface import IO_Operate, File_Info, parsed_spectrum_queue, spectrum_static_info, pre_writed_frame_queue, pre_parsed_demodulate_frame_queue, parsed_demodulate_queue
from commom import sliding_window, array_splice_by_threshold, check_and_list_dir, random_filename, read_json_file, write_json_file, check_file_status
from file_operate import save_file
from threading import Thread
from sql_operation.sql import Sql
from sql_operation.orm import query_file, insert_file, insert_instruction
from XMLParse.write_xml import WriteXML
from XMLParse.parse_xml import ParseXML
from TCP_SOCKET_IO.tcp_client import TCPClient
from auth import auth
from coding import coding
from setting import setting
from utils.variable import CODE_T_QUEUE, PROFILE_CONFIG_M_QUEUE
# from config import config
from exts import creat_app, db
from models import Files, Instruction
from  datetime import datetime

# app = Flask(__name__)
app = creat_app()
app.app_context().push()

# app.config['SECRET_KEY'] = os.urandom(24)
# app.config.from_object(config["dev_config"])
# register blueprint
app.register_blueprint(auth, url_prefix='/dev-api/user')
app.register_blueprint(coding, url_prefix='/dev-api/coding')
app.register_blueprint(setting, url_prefix='/dev-api/configure')

# init db
# db.init_app(app)


# base url
BASE_URL = r'http://192.168.1.198:5000/static/dat_file/spectrum/xx'

# socketio = SocketIO(cors_allowed_origins="*", async_mode='gevent')
socketio = SocketIO(cors_allowed_origins="*", async_mode=None)
socketio.init_app(app)
# socketio = SocketIO(app, cors_allowed_origins="*")


##############################
# alias modify container
ALIAS_DICT = dict()

# file save operation
FILE_SAVE = None

parse_souce_data = None


save_frame = b''

CLIENT_SIDS = {}

CODING_INFO = None


SAVE_FLAG = False
UPLOAD_XML_FOLD = r'./static/xml_file/upload'
M_QUEUE_MAX_SIZE = 10000        # decoding process function Queue's max size
SUB_THREAD_QUEUE_MAX_SIZE = 1000

LOCAL_XML_URL = r'http://127.0.0.1:5000/static/zip_file/xx'


@app.route('/')
def index():
    # return response_wrapper('hello world', 'get')
    return render_template('index.html')


@app.route('/get_coding_log', methods=['get'])
def get_coding_log():
    # if request.method == 'OPTIONS':
    #     return response_wrapper((None, 200), 'options')
    if request.method == 'GET':
        # paras = json.loads(request.get_data())
        # paras = request.args
        try:
            page_number = int(request.args.get('pageNumber'))
            page_size = int(request.args.get('pageSize'))
        except KeyError:
            print('<get coding log, get KeyError！>')
            return response_wrapper('error', 'get')
        else:
            sql = Sql()
            logs = sql.search_coding_log(sql.command, page_number, page_size, 'log_coding')
            return response_wrapper({'data': logs, 'coding_info': CODING_INFO}, 'get')


# @app.route('/uploadXml', methods=['POST', 'OPTIONS'])
# def upload_xml():
#     """this function used to save the template information VUE"""
#     if request.method == 'OPTIONS':
#         return response_wrapper((None, 200), 'OPTIONS')
#
#     if request.method == 'POST':
#         xmls = request.files.getlist('xml')
#         for file in xmls:
#             filename = secure_filename(random_filename(file.filename))
#             if filename == '':
#                 print('file upload failed!')
#                 continue
#             if filename.endswith('.xml'):
#                 file.save(os.path.join(r'./static/xml_file/upload', filename))
#                 insert_sql_one_by_one(os.path.join(r'./static/xml_file/upload', filename))
#             else:
#                 print('upload file {} is not xml file!'.format(filename))
#         return response_wrapper('success', 'post')
#
#
# def insert_sql_one_by_one(file_path: str):
#     try:
#         if os.path.exists(file_path):
#             xml = ParseXML(file_path).xml_info
#             sql = Sql()
#             sql.insert_decoding_data(sql.command, xml)
#
#     except Exception as e:
#         raise e
#     else:
#         print("upload xml files insert into sql sueecss!")


def insert_sql():               # pending
    """notice: because of vue's upload component, this function pending"""
    try:
        xml_files = check_and_list_dir(UPLOAD_XML_FOLD, 'xml')
        xmls = ParseXML(xml_files).parse_xml_into_array()
        sql = Sql()
        for xml in xmls:
            sql.insert_decoding_data(sql.command, xml)
    except Exception as e:
        raise e
    else:
        print("upload xml files insert into sql sueecss!")


@app.route('/export_xml', methods=['POST', 'options'])
def export_xml():
    # if request.method == 'GET':
    #     sql = Sql()
    #     sql.search(sql.command, '2020-03-18 20:52:35', '2020-03-18 20:52:40')
    #     xml = WriteXML(sql.encoding_xml_array)
    #     xml.write_xml_file()
    """this function used to save the template information VUE"""
    if request.method == 'OPTIONS':
        return response_wrapper((None, 200), 'OPTIONS')

    if request.method == 'POST':

        result = json.loads(request.get_data())
        try:
            start_time = result['start_time']
            stop_time = result['stop_time']
        except KeyError:
            print('export_xml function has key error!')
        else:
            sql = Sql()
            sql.search(sql.command, start_time, stop_time)
            xml = WriteXML(sql.deconding_xml_array)
            xml.write_xml_file()

        # 判断是否存在当前 user id 路径
            zip_file = os.path.join(r'./static/zip_file/', xml.xml_file_name.split('.')[0]+'.zip')
            if os.path.exists(zip_file):
                return response_wrapper(urljoin(LOCAL_XML_URL, xml.xml_file_name.split('.')[0]+'.zip'), 'post')
            else:
                return response_wrapper('error', 'post')


@app.route('/get_test', methods=['GET'])
def get_test():
    if request.method == 'GET':
        print("this is get request")
        return response_wrapper('received the get request', 'get')


@socketio.on('disconnect')
def disconnect_handler():
    print("Client Disconnected!")
    if request.sid in CLIENT_SIDS:
        CLIENT_SIDS.pop(request.sid)


def ack(data):
    print('message was received!: %s' % data)


@socketio.on('connect')
def connect_handler():
    print("Client connected!")
    if request.sid not in CLIENT_SIDS:
        CLIENT_SIDS[request.sid] = {}


def server_ack(title: str, message: str, types: str, data: list = None):
    """client emit callback message"""
    return (title, message, types, data)


@socketio.on('aliasModify')
def modify_alias(data):
    print('---------{}----------'.format(data))
    ALIAS_DICT.update(data)
    write_json_file(ALIAS_DICT)


@socketio.on('demodeChannelSelect')
def demode_channel_select(data):
    # a: I channel b: Q channel d: default(all channel)
    print('---- 解调通道选择-----{}--------'.format(data))


@socketio.on("startSaveFile")
def start_save_file(param):
    """

    :param data:
    :return:
    """
    try:
        f = deepcopy(FILE_SAVE)
        _ = param["id"]
        if _ == "spectrum":
            # FILE_SAVE_FLAG["spectrum"] = True
            f["flag"].update({"spectrum": True})
            # pp[0].send(FILE_SAVE_FLAG)
        elif _ == "demode_rp":
            # FILE_SAVE_FLAG["demode_rp"] = True
            f["flag"].update({"demode_rp": True})
        elif _ == "demode_yp":
            # FILE_SAVE_FLAG["demode_yp"] = True
            f["flag"].update({"demode_yp": True})
        elif _ == "decoding":
            # FILE_SAVE_FLAG["decoding"] = True
            f["flag"].update({"decoding": True})
    except Exception as e:
        print("<start save file error!- %s>" % e)
    else:
        FILE_SAVE.update(f)
        return server_ack("成功", "指令发送成功！", "success")


@socketio.on("stopSaveFile")
def stop_save_file(param):
    """
    :
    :param data: type is dict
    :return:
    """
    try:
        f = deepcopy(FILE_SAVE)
        _ = param["id"]
        if _ == "spectrum":
            # FILE_SAVE_FLAG["spectrum"] = False
            f["flag"].update({"spectrum": False})
        elif _ == "demode_rp":
            # FILE_SAVE_FLAG["demode_rp"] = False
            f["flag"].update({"demode_rp": False})
        elif _ == "demode_yp":
            # FILE_SAVE_FLAG["demode_yp"] = False
            f["flag"].update({"demode_yp": False})
        elif _ == "decoding":
            # FILE_SAVE_FLAG["decoding"] = False
            f["flag"].update({"decoding": False})
    except Exception as e:
        print("<stop save file error!- %s>" % e)
    else:
        FILE_SAVE.update(f)
        return server_ack("成功", "指令发送成功！", "success")


@socketio.on('getSpectrumFiles')
def get_spectrum_files():
    """get the spectrum files list and return the result to the frontend"""
    fold_path = os.path.join(r'./static/dat_file', 'spectrum')
    try:
        files = os.listdir(fold_path)
    except FileNotFoundError as e:
        print('<频谱数据文件获取异常！{}>'.format(e))
        return server_ack("异常", "频谱数据文件列表获取异常！", "error")
    else:
        files_list = list()
        for f in files:
            file = os.path.join(fold_path, f)
            file_info = dict()  # used to save single file information
            file_info['fileName'] = f.strip().split('.')[0]  # get the cfg name
            file_info['fileStatus'] = "完成" if check_file_status(file) else "写入中"
            file_info['fileSize'] = os.path.getsize(file) / 1024  # KB (1024)
            file_info['creatTime'] = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(os.path.getctime(file)))
            file_info['filePath'] = f
            files_list.append(file_info)
        return server_ack("成功", "已获取频谱数据文件列表！", "success", files_list)


@socketio.on('downloadFile')
def download_file(param: dict):
    fold_path = os.path.join(r'./static/dat_file', 'spectrum')
    if os.path.isfile(os.path.join(fold_path, param["file"])):      # param["file"]: file name
        try:
            return server_ack("成功", "文件下载请求成功！", "success", urljoin(BASE_URL, param["file"]))
        except Exception as e:
            print("文件下载异常！- %s" % e)
            return server_ack("异常", "文件下载请求异常！", "error", None)


@socketio.on('deleteFile')
def delete_file_wrapper(*args):
    res_queue = T_Queue(1)
    t = Thread(target=delete_file, args=(*args,), kwargs={'res_queue': res_queue}, daemon=True)
    t.start()
    # t.join()
    res = res_queue.get()
    return res


def delete_file(param: dict, res_queue=None):
    """the file delete function"""
    fold_path = os.path.join(r'./static/dat_file', 'spectrum')
    if os.path.isfile(os.path.join(fold_path, param["file"])):  # param["file"]: file name
        try:
            os.remove(os.path.join(fold_path, param["file"]))
            # redirect(url_for('get_spectrum_files'))      # @app.route 下操作
            res = list(get_spectrum_files())
            res[1] = "文件删除请求成功！"
            res_queue.put(server_ack(*res))
        except Exception as e:
            print("文件下载异常！- %s" % e)
            res_queue.put(server_ack("异常", "文件删除请求异常！", "error", None))


@socketio.on('setSpectrum')
def set_spectrum(data):
    """用于频谱设置指令使用，提取对应的起止频率信息 None 为默认值"""
    # global lower_seted_spectrum
    # global higher_seted_spectrum

    CLIENT_SIDS[request.sid]['lower_seted_spectrum'] = float(data['lower']) if data['lower'] else -float('Inf')
    CLIENT_SIDS[request.sid]['higher_seted_spectrum'] = float(data['higher']) if data['higher'] else float('Inf')
    return server_ack("成功", "频谱设置命令发送成功！", "success")


def real_time_demode_rp(server_addr: str, server_port: int, buffer_size: int = 32768):
    """
        : real time spectrum data process
        :param server_addr:
        :param server_port:
        :param buffer_size:
        :return:
        """

    client = TCPClient(server_addr, int(server_port), int(buffer_size))
    save_queue = M_Queue(500)
    file = File_Info(r'')
    io = IO_Operate(file.path)
    io.real_time_demodulation_run(client.translate_data,
                                  io.io_bytes_queue,
                                  io.io_data_queue,
                                  save_queue,
                                  None,
                                  True,
                                  True)

    # write_file = Process(target=save_file, args=(save_queue, FILE_SAVE_FLAG, 102400), daemon=True)
    write_file = Process(target=save_file,
                         args=("demode_rp", save_queue, PROFILE_CONFIG_M_QUEUE, FILE_SAVE, 102400, r'./static/dat_file/demodulate/rp'),
                         daemon=True)
    write_file.start()

    while True:
        try:
            item = io.io_data_queue.get()
            body_info = item["body_info"]
            # header_info = item["header_info"]

        except Exception as e:
            print("<real time spectrum get has error!>-%s" % e)

        # result = {
        #     'xAxis': {
        #         'start': header_info['start_spectrum'],
        #         'stop': header_info['stop_spectrum']
        #     },
        #     'yAxis': body_info
        # }
        # print("============", {
        #     'start': header_info['start_spectrum'],
        #     'stop': header_info['stop_spectrum']
        # }, sum(body_info))
        socketio.emit('demodulateMsg', body_info[:2000])
        print(len(body_info))
        del body_info
        del item
        time.sleep(0.8)
        # print('one frame of demodulate data emit success!')


def real_time_spectrum(server_addr: str, server_port: int, buffer_size: int = 32768):
    """
    : real time spectrum data process
    :param server_addr:
    :param server_port:
    :param buffer_size:
    :return:
    """

    client = TCPClient(server_addr, int(server_port), int(buffer_size))
    save_queue = M_Queue(500)
    file = File_Info(r'')
    io = IO_Operate(file.path)
    # io.real_time_spectrum_run(client.translate_data, io.io_bytes_queue, [])

    io.real_time_spectrum_run(client.translate_data,
                              io.io_bytes_queue,
                              save_queue,
                              CODE_T_QUEUE,           # []
                              True,
                              True)


    """ 修改设计思路为：tcp 一直进行存储队列接收。 当为保持flag时，进行写文家保存，不为保存flag时，不写入文件。"""

    write_file = Process(target=save_file,
                         args=("spectrum", save_queue, PROFILE_CONFIG_M_QUEUE, FILE_SAVE, 102400, r'./static/dat_file/spectrum'),
                         daemon=True)
    write_file.start()

    while True:
        try:
            # print(CODE_T_QUEUE.get())
            item = io.io_data_queue.get(block=False)        #
            body_info = item["body_info"]
            header_info = item["header_info"]
        except KeyError:
            print('<real time spectrum parse parameters error!>')
        except Exception:
            # print("<real time spectrum get has error!>-%s" % e)
            pass
        else:
            result = {
                        'xAxis': {
                            'start': header_info['start_spectrum'],
                            'stop': header_info['stop_spectrum']
                        },
                        'yAxis': body_info
            }
            # print("============", {
            #                 'start': header_info['start_spectrum'],
            #                 'stop': header_info['stop_spectrum']
            #             }, sum(body_info))
            socketio.emit('message', result)
            print('put--{}---get--{}'.format(io.io_bytes_queue.qsize(), io.io_data_queue.qsize()))
            del header_info
            del body_info
            del item
            time.sleep(0.9)
            # print('one real time frame of spectrum emit success!')


@socketio.on('leave')
def leave_handler():
    leave_room(request.sid)
    print("{} has leaved the room!".format(request.sid))


@socketio.on('join')
def join_handler(param):
    join_room(request.sid)
    # sid = request.sid
    # if request.sid not in session:
    request.sid = Thread(target=replay, args=(request.sid, param['file']), daemon=True)
    request.sid.start()
    # session[request.sid] = request.sid


def replay(sid, file):
    """
    :
    :param sid:
    :param file: the full path of the replay file
    :return:
    """
    # used to replay data for single sid
    fold_path = os.path.join(r'./static/dat_file', 'spectrum')
    f = File_Info(os.path.join(fold_path, file))
    io = IO_Operate(f.path, 'rb')
    io.replay_time_spectrum_run()
    while True:
        try:
            item = io.io_data_queue.get()
            body_info = item["body_info"]
            header_info = item["header_info"]
        except Exception as e:
            print("取空！")
        try:
            lower_spectrum = CLIENT_SIDS[sid]['lower_seted_spectrum']
            higher_spectrum = CLIENT_SIDS[sid]['higher_seted_spectrum']
        except KeyError:
            lower_spectrum = float('-inf')
            higher_spectrum = float('inf')

        if lower_spectrum or higher_spectrum:
            step = (header_info['stop_spectrum'] - header_info['start_spectrum']) / len(body_info)
            source_x = [(round(header_info['start_spectrum'] + step*x, 2))
                        for x in range(len(body_info))]

            x_start = lower_spectrum if lower_spectrum != float('-inf') else header_info["start_spectrum"]

            if header_info['start_spectrum'] > higher_spectrum or header_info['stop_spectrum'] < lower_spectrum:
                print("错误", "设置频率不在有效频率范围内！")
                CLIENT_SIDS[sid]['lower_seted_spectrum'] = float('-inf')
                CLIENT_SIDS[sid]['higher_seted_spectrum'] = float('inf')
                continue

            x_data, index_from, index_to = array_splice_by_threshold(source_x, lower_spectrum, higher_spectrum)
            value = sliding_window(body_info[index_from:index_to], len(x_data))
            x_step = (x_data[-1] - x_data[0]) / len(value)
            y_data = [value[x + 1] for x in range(len(value) - 1)]
        else:
            value = sliding_window(body_info, len(body_info), 2)
            x_start = header_info['start_spectrum']
            x_step = (header_info['stop_spectrum'] - header_info['start_spectrum']) / len(value)
            y_data = [value[x + 1] for x in range(len(value) - 1)]

        x_result = {'start': (round(x_start, 2)), 'step': x_step}
        socketio.emit('replayMessage', {'xAxis': x_result, 'yAxis': y_data}, room=sid)
        time.sleep(0.8)
        print('one frame of spectrum emit success!')


# @socketio.on('replaySpectrum')
# def replaySpectrum(file_path):
#     emit('replay', [1, []])
#     sid = request.sid
#     file = file_path
#     file = File_Info(r'E:\share-198\57\sp.dat')
#     io_repay = IO_Operate('rb', file.path)
#     # io_repay.replay_time_run()
#     while True:
#         try:
#             parse_souce_data = io_repay.io_data_queue.get()
#         except Exception as e:
#             print("取空！")
#         value = sliding_window(parse_souce_data, io_repay.io_static_info['spectrum_num'], 2)
#         print(value[-10:])
#         x_step = (io_repay.io_static_info['stop_spectrum'] - io_repay.io_static_info['start_spectrum']) / \
#                      len(value)
#         result = [[(round(io_repay.io_static_info['start_spectrum'] + x_step * x, 2)), value[x + 1]] for x in
#                   range(len(value) - 1)]
#
#         if not result:
#             print("result 为空")
#         emit('replayMessage', [1, result])


def decoding_fun(queue, sub_thread_queue):
    """
    :: insert the parsed decoding data into sql
    :param queue:  processing queue，used to caching the decoding
    data(object structure commonly can without endswith sign(+,- EWSN))
    :param sub_thread_queue: the message queue use to trans message to gevent thread to emit the message
    :return:
    """

    sql = Sql()
    # with app.app_context():
    while True:
        try:
            item = queue.get()
            if isinstance(item, dict):
                sql.insert_decoding_data(sql.command, item)         # insert the message into the sql

                sub_thread_queue.put(item)      # use to translate message to the main gevent thread
        except Exception as e:
            print('<multi process decoding_fun error!{}>'.format(e))

        time.sleep(2)


def test_fun(queue):
    xml_file1 = r'C:\Users\Lenovo\Pictures\57\分析应用软件参考文档，已脱密\verson2\5F1_01_19707000_20200325100509_0001.xml'
    xml_file2 = r'C:\Users\Lenovo\Pictures\57\分析应用软件参考文档，已脱密\verson2\5F1_01_19707000_20200325100509_0002.xml'
    # xml_file2 = r'C:\Users\Lenovo\Pictures\57\分析应用软件参考文档，已脱密\GSC_DDC2100k_signalling（2）.xml'
    xml1 = ParseXML(xml_file1).xml_info
    xml2 = ParseXML(xml_file2).xml_info
    # sql = Sql()
    index = 0
    while True:
        index += 1
        if index % 2 == 0:
            queue.put(xml1)
        else:
            queue.put(xml2)
        time.sleep(0.5)


def emit_message(decoding_queue):
    while True:
        try:
            message = decoding_queue.get()

        except Exception as e:
            print('<emit message thread "queue.get" error!{}>'.format(e))
        else:
            # if message:
            gateway = "关口站" + message["关口站"]["经度"]+message["关口站"]["纬度"]
            dev_location = "设备位置" + message["设备位置"]["经度"] + message["设备位置"]["纬度"]

            if ALIAS_DICT and gateway in ALIAS_DICT:
                message["关口站"]["备注"] = ALIAS_DICT[gateway]

            if ALIAS_DICT and dev_location in ALIAS_DICT:
                message["设备位置"]["备注"] = ALIAS_DICT[dev_location]

            socketio.emit('decodingMessage', message)
            print('decoding message emit success!')


def demode_file_operation_yp(server_addr: str, server_port: int, buffer_size: int = 32768):

    client = TCPClient(server_addr, int(server_port), int(buffer_size))
    save_queue = M_Queue(500)

    p = Process(target=client.translate_data,
                args=(None, save_queue, None, False, True),
                daemon=True)

    write_file = Process(target=save_file,
                         args=("demode_yp", save_queue, PROFILE_CONFIG_M_QUEUE, FILE_SAVE, 102400, r'./static/dat_file/demodulate/yp'),
                         daemon=True)

    p.start()
    write_file.start()


# @app.before_first_request
def app_init():
    # 文件存储全局状态初始化
    try:
        r = query_file(Files)
    except:
        try:
            db.create_all(app=creat_app())
            insert_file(db, Files, 100*1024**2, 100*1024**2, 100*1024**2, 100*1024**2, True, True, True, False)
            db.session.commit()
        except Exception as e:
            print("<model Files creat_all has exception! - {}>".format(e))
            db.session.rollback()
        finally:
            # default file operation information
            k = {
                "flag": {
                    "spectrum": True,  # 9000
                    "demode_rp": True,  # 9900
                    "demode_yp": True,  # 9990
                    "decoding": False  # 9999
                },
                "size": {
                    "spectrum": 100*1024**2,  # 100 MB
                    "demode_rp": 100*1024**2,
                    "demode_yp": 100*1024**2,
                    "decoding": 100*1024**2
                }
            }

    else:
        k = {"flag": {}, "size": {}}
        k["flag"].update(spectrum=r[0])     # if save data flag
        k["flag"].update(demode_rp=r[1])
        k["flag"].update(demode_yp=r[2])
        k["flag"].update(decoding=r[3])

        k["size"].update(spectrum=r[4])     # the file split size
        k["size"].update(demode_rp=r[5])
        k["size"].update(demode_yp=r[6])
        k["size"].update(decoding=r[7])

    # global FILE_SAVE_FLAG, FILE_SAVE_SIZE
    # FILE_SAVE_FLAG = Manager().dict({
    #     "spectrum": True,       # 9000
    #     "demode_rp": True,      # 9900
    #     "demode_yp": True,      # 9990
    #     "decoding": False       # 9999
    # })  # the file saving flag(true: save, false: none save),  default is false;
    #
    # FILE_SAVE_SIZE = Manager().dict({
    #     "spectrum": 100*1024**2,        # 100 MB
    #     "demode_rp": 100*1024**2,
    #     "demode_yp": 100*1024**2,
    #     "decoding": 100*1024**2
    # })
    global FILE_SAVE
    # FILE_SAVE = Manager().dict({
    #     "flag": {
    #         "spectrum": True,       # 9000
    #         "demode_rp": True,      # 9900
    #         "demode_yp": True,      # 9990
    #         "decoding": False       # 9999
    #     },
    #     "size": {
    #         "spectrum": 100 * 1024 ** 2,  # 100 MB
    #         "demode_rp": 100 * 1024 ** 2,
    #         "demode_yp": 100 * 1024 ** 2,
    #         "decoding": 100 * 1024 ** 2
    #     }
    # })
    FILE_SAVE = Manager().dict(k)       # creat the variable exchange between multiple threading

    # demode data(yp) just save
    # demode_file_operation_yp('192.168.1.198', 9990, 32*1024)

    # 读取指令配置信息
    global CODING_INFO
    CODING_INFO = read_json_file(r'./static/config/coding', 'code_info.cfg')
    # 读取 profile alias设置
    global ALIAS_DICT
    ALIAS_DICT = read_json_file(r'./static/config/profile', 'profile.cfg')

    # real time processing of spectrum
    spectrum = Thread(target=real_time_spectrum, args=('192.168.1.198', 9000, 32*1024), daemon=True)
    spectrum.start()

    # 启动独立进程进行接调数据处理
    demode_rp = Thread(target=real_time_demode_rp, args=('192.168.1.198', 9900, 32*1024), daemon=True)
    # demode_rp.start()


    # """  20200423

    # 启动回放数据
    # file = File_Info(r'D:\python_project\flask_57\flask_server\static\dat_file\sp.dat')
    # io_repay = IO_Operate('rb', file.path)
    # io_repay.run_replay()
    # producer = Thread(target=read_data, daemon=True)
    # producer.start()

    # 启动独立进程进行译码数据处理
    m_queue = M_Queue(M_QUEUE_MAX_SIZE)     # multiprocessing queue
    sub_queue = M_Queue(SUB_THREAD_QUEUE_MAX_SIZE)      # ues to emit message to client
    test = Process(target=test_fun, args=(m_queue, ), daemon=True)
    decoding_process = Process(target=decoding_fun, args=(m_queue, sub_queue), daemon=True)
    # test.start()
    # decoding_process.start()




    # 启动独立线程，用于接收子进程中的消息并emit
    subThread = Thread(target=emit_message, args=(sub_queue,), daemon=True)
    # subThread.start()

# """


if __name__ == '__main__':
    # if os.environ.get('WERKZEUG_RUN_MAIN') == 'true':
    app_init()
    socketio.run(app, host='0.0.0.0', debug=False, use_reloader=False)
