#!/usr/bin/env python3
# -*- encoding:utf-8 -*-

from . import auth
import json
from flask import request, session
from corsSupport import response_wrapper
from werkzeug.security import generate_password_hash, check_password_hash


@auth.route('/login', methods=['options', 'post'])
def user_login():
    if request.method == "OPTIONS":
        return response_wrapper((None, 200), 'options')

    if request.method == "POST":
        users_name = ["admin"]
        hash_password = generate_password_hash('123456')
        res = json.loads(request.get_data())

        if check_password_hash(hash_password, res["password"]) and res["username"] in users_name:
            session["username"] = res["username"]
            session["password"] = res["password"]
            return response_wrapper({"status": "success",
                                     "info": "登陆成功！",
                                     "code": 20000,
                                     "data": {"token": "caoy@123"}},
                                    'post')
        else:
            return response_wrapper({"status": "error",
                                     "info": "登陆失败！",
                                     "code": 20000},
                                    'post')


@auth.route('/logout', methods=['options', 'post'])
def user_logout():
    if request.method == "OPTIONS":
        return response_wrapper((None, 200), 'options')

    if request.method == "POST":
        session.pop("username", None)
        session.pop("password", None)
        return response_wrapper({"status": "success", "info": "退出登陆！", "code": 20000},
                                'post')


@auth.route('/info', methods=['GET'])
def user_info():
    """
    : request.method == 'GET', frontend use to get the user's information
    :return:
    """
    if request.method == 'GET':
        d = dict()
        d["code"] = 20000
        d["data"] = {"avatar": request.environ["wsgi.url_scheme"] +
                               "://" +
                               request.environ["HTTP_HOST"] +
                               '/static/assert/user.jpg',
                     "introduction": 'I am the administrator',
                     "name": "administrator",
                     "roles": ["admin"]}

        return response_wrapper(d, 'get')
