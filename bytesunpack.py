import struct

# def get_bytes(data, cfg):
#     # assert cfg['offset'] % 8 == 0
#     # assert cfg['len'] % 8 == 0
#
#     start = cfg['offset']//8
#     end = start + (cfg['len'])//8
#
#     return data[start:end]
#
#
# def get_value(data, cfg):
#     #assert (cfg['len']) <= 64
#     #TODO 长度大于64字节的情况...
#     if cfg['len'] > 64:
#         return 0
#
#     sp = (cfg['offset'])
#     ep = sp + (cfg['len'])
#     start = sp//8
#     end = ep//8
#     rpadding = ep%8
#
#     if rpadding > 0:
#         end += 1
#         rpadding = 8 - rpadding
#
#     fullbytes = data[start:end]
#     fullint = int.from_bytes(fullbytes, byteorder='big')
#     mask = (1 << (cfg['len'])) - 1
#
#     return (fullint >> rpadding) & mask
#
#
# def get_hex(data, cfg):
#     return hex(get_value(data, cfg)).upper()
#


def bytes_to_int(frame, endian: str, signed: bool = True):
    """
    :字节->int, bytes 长 由输入确定
    :change byte to int
    :param frame:
    :param endian: little or big
    :param signed:
    :return:
    """
    return int.from_bytes(frame, endian.lower(), signed=signed)


def bytes_to_float(frame: bytes, endian: str):
    """
    :bytes to float
    :param frame:
    :param endian:
    :return:
    """
    return struct.unpack(
        '>f',
        frame)[0] if endian.lower() == 'big' else struct.unpack(
        '<f',
        frame)[0]
