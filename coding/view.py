#!/usr/bin/env python3
# -*- encoding:utf-8 -*-
# Author:caoy
# @Time:  12:32


from . import coding
import json
import os
from datetime import datetime
from flask import request
from flask_cors import cross_origin
from werkzeug.utils import secure_filename
from urllib.parse import urljoin
from commom import random_filename
from corsSupport import response_wrapper
from utils.variable import CODE_M_QUEUE, CODE_T_QUEUE, LOCAL_XML_URL
from models import Instruction
from sql_operation.orm import insert_instruction, query_instruction
from exts import db
from sql_operation.sql import Sql, ParseXML
from XMLParse.write_xml import WriteXML


@coding.route('creatLinking', methods=['options', 'post'])
def creat_link():
    if request.method == 'OPTIONS':
        return response_wrapper((200, None), 'OPTIONS')

    if request.method == 'POST':
        res = json.loads(request.get_data())
        form = res["form"]
        instruction = res["instruction"]
        one_byte = 0xFF
        try:
            soft_channel = form["channelSoft"]   # 软件通道
            hard_channel = form["channelHard"]   # 硬件通道
            timing = form["timingSelection"]     # 时序选择
            center_spectrum = form["centerSpectrum"]     # 中心频点
            freq = form["freqConversion"]    # 变频关系（GHz）
            data_collect = form["dataCollection"]    # 数据采集
            gain = form["gain"]  # 增益控制 (手动/自动)
            gain_value = form["gainValue"]  # 增益值(db)

            # get the 128 bytes code frame
            frame = 'A9CEC4E9'
            frame += 'FF'*30  # 保留 4 + 时间戳 8 + 微妙脉冲计数 4 + 经纬度 8+ 保留 6
            frame += '01'if soft_channel == 'A' else '02'   # 软件通道号 1

            # AD通道号 排序时间读取指示 射频频段 1
            one_byte = one_byte & 0x00 if hard_channel == 'A' else one_byte & 0x40
            if timing == 'A':       # windows 时间表示法
                one_byte = one_byte & 0x00
            elif timing == 'B':     # GPS/北斗
                one_byte = one_byte & 0x01
            elif timing == 'C':     # B码
                one_byte = one_byte & 0x20
            elif timing == 'D':     # 脉冲计数器
                one_byte = one_byte & 0x30
            frame_bytes = bytes.fromhex(frame) + one_byte.to_bytes(1, 'little')

            # 中心频点 unsigned int little endian
            frame_bytes += int(center_spectrum).to_bytes(4, 'little')

            # 射频变频关系
            if freq == 'A':
                frame = '00'
            elif freq == 'B':
                frame = '01'
            elif freq == 'C':
                frame = '02'

            # 保留 1
            frame += 'FF'

            # 数据采集输出数据类型 1
            # if data_collect == 'A':     # 表示AD1采样基带DDC输出数据
            #     frame += 'A1'
            # elif data_collect == 'B':   # 表示AD2采样基带DDC输出数据
            #     frame += 'A2'
            # elif data_collect == 'C':   # 表示通道1解调输出数据
            #     frame += 'D1'
            # elif data_collect == 'D':   # 表示通道1译码输出数据
            #     frame += 'D2'
            # elif data_collect == 'E':   # 表示通道2解调输出数据
            #     frame += 'D3'
            # elif data_collect == 'F':   # 表示通道2译码输出数据
            #     frame += 'D4'
            frame += 'FF'

            # 增益 1 byte + 能量值 4
            frame += 'FF'*5

            # 信令控制命令类型 1
            frame += '01'

            # 硬件配控响应 1 + 保留 3
            frame += 'FF' * 4

            # 数据采集使能 1
            one_byte = 0x00     # 全不采集
            one_byte = one_byte | 0x01 if 'A' in data_collect else one_byte & 0xFE  # 解调软判
            one_byte = one_byte | 0x02 if 'B' in data_collect else one_byte & 0xFD  # 解调硬判
            one_byte = one_byte | 0x04 if 'C' in data_collect else one_byte & 0xFB  # 译码
            one_byte = one_byte | 0x08 if 'D' in data_collect else one_byte & 0xF7  # 频谱
            frame_bytes += bytes.fromhex(frame) + one_byte.to_bytes(1, 'little')


            # 增益方式（手动/自动）
            frame = '00' if gain == 'A' else '01'  # A-0 自动

            # 增益值
            frame_bytes += bytes.fromhex(frame) + int(gain_value).to_bytes(1, 'little')

            frame_bytes += bytes.fromhex('FF'*72)

            CODE_T_QUEUE.put(frame_bytes)
            # insert instruction information into sql
            parms = datetime.now(), instruction["inst_name"], instruction["inst_key"], \
                    instruction["inst_from"], instruction["inst_to"], instruction["inst_status"]
            insert_instruction(db, Instruction, *parms)

        except KeyError:
            print('<解译建链指令参数解析异常！>')
            # code:
            return response_wrapper({"code": 20000,
                                     "data": {"title": "指令发送提示", "message": "指令参数解析错误！", "type": "error"}})

        else:
            return response_wrapper({"code": 20000}, 'POST')


@coding.route('removeLinking', methods=["options", "post"])
def remove_link():
    if request.method == 'OPTIONS':
        return response_wrapper((200, None), "post")

    if request.method == "POST":
        res = json.loads(request.get_data())
        form = res["form"]
        instruction = res["instruction"]
        one_byte = 0xFF
        try:
            soft_channel = form["channelSoft"]  # 软件通道
            hard_channel = form["channelHard"]  # 硬件通道

            # get the 128 bytes code frame
            frame = 'A9CEC4E9'
            frame += 'FF' * 30  # 保留 4 + 时间戳 8 + 微妙脉冲计数 4 + 经纬度 8+ 保留 6
            frame += '01' if soft_channel == 'A' else '02'  # 软件通道号 1

            # AD通道号
            one_byte = one_byte & 0x00 if hard_channel == 'A' else one_byte & 0x40

            frame_bytes = bytes.fromhex(frame) + one_byte.to_bytes(1, 'little')

        # 通道i中心频点 4 + 射频变频关系 1 + 保留 1 + 采集输出数据类型 1 + 增益 1 + 能量值（float）4
            frame = 'FF'*12

            # 信令控制命令类型
            frame += '03'

            frame_bytes += bytes.fromhex(frame + 'FF'*79)

            CODE_T_QUEUE.put(frame_bytes)
            # insert instruction information into sql
            parms = datetime.now(), instruction["inst_name"], instruction["inst_key"], \
                    instruction["inst_from"], instruction["inst_to"], instruction["inst_status"]
            insert_instruction(db, Instruction, *parms)


        except KeyError:
            print('<解译建链指令参数解析异常！>')
            # code:
            return response_wrapper({"code": 20000,
                                     "data": {"title": "指令发送提示", "message": "指令参数解析错误！", "type": "error"}})

        else:
            return response_wrapper({"code": 20000}, 'POST')


@coding.route('collectSpectrum', methods=["options", "post"])
def collect_spectrum():
    if request.method == 'OPTIONS':
        return response_wrapper((200, None), "post")

    if request.method == "POST":
        res = json.loads(request.get_data())
        form = res["form"]
        instruction = res["instruction"]
        one_byte = 0xFF
        try:
            soft_channel = form["channelSoft"]  # 软件通道
            hard_channel = form["channelHard"]  # 硬件通道
            start_spectrum = form["startSpectrum"]  # 硬件通道
            stop_spectrum = form["stopSpectrum"]  # 硬件通道

            # get the 128 bytes code frame
            frame = 'A9CEC4E9'
            frame += 'FF' * 30  # 保留 4 + 时间戳 8 + 微妙脉冲计数 4 + 经纬度 8+ 保留 6
            frame += '01' if soft_channel == 'A' else '02'  # 软件通道号 1

            # AD通道号
            one_byte = one_byte & 0x00 if hard_channel == 'A' else one_byte & 0x40

            frame_bytes = bytes.fromhex(frame) + one_byte.to_bytes(1, 'little')

            # 通道i中心频点 4 + 射频变频关系 1 + 保留 1 + 采集输出数据类型 1 + 增益 1 + 能量值（float）4
            frame = 'FF' * 12

            # 信令控制命令类型
            frame += '05'  # 开启频谱采集

            frame += 'FF'*15

            # 频谱起始频率
            frame_bytes += bytes.fromhex(frame) + int(start_spectrum).to_bytes(2, 'little')
            # 频谱结束频率
            frame_bytes += int(stop_spectrum).to_bytes(2, 'little')
            frame_bytes += bytes.fromhex('FF'*60)

            CODE_T_QUEUE.put(frame_bytes)
            # insert instruction information into sql
            parms = datetime.now(), instruction["inst_name"], instruction["inst_key"], \
                    instruction["inst_from"], instruction["inst_to"], instruction["inst_status"]
            insert_instruction(db, Instruction, *parms)
        except KeyError:
            print('<解译建链指令参数解析异常！>')
            # code:
            return response_wrapper({"code": 20000,
                                     "data": {"title": "指令发送提示", "message": "指令参数解析错误！", "type": "error"}}, 'post')

        else:
            return response_wrapper({"code": 20000}, 'POST')


@coding.route('stopSpectrum', methods=["options", "post"])
def stop_spectrum():
    if request.method == 'OPTIONS':
        return response_wrapper((200, None), "post")

    if request.method == "POST":
        res = json.loads(request.get_data())
        form = res["form"]
        instruction = res["instruction"]
        one_byte = 0xFF
        try:
            soft_channel = form["channelSoft"]  # 软件通道
            hard_channel = form["channelHard"]  # 硬件通道

            # get the 128 bytes code frame
            frame = 'A9CEC4E9'
            frame += 'FF' * 30  # 保留 4 + 时间戳 8 + 微妙脉冲计数 4 + 经纬度 8+ 保留 6
            frame += '01' if soft_channel == 'A' else '02'  # 软件通道号 1

            # AD通道号
            one_byte = one_byte & 0x00 if hard_channel == 'A' else one_byte & 0x40

            frame_bytes = bytes.fromhex(frame) + one_byte.to_bytes(1, 'little')

            # 通道i中心频点 4 + 射频变频关系 1 + 保留 1 + 采集输出数据类型 1 + 增益 1 + 能量值（float）4
            frame = 'FF' * 12

            # 信令控制命令类型
            frame += '07'           # 关闭频谱采集

            frame_bytes += bytes.fromhex(frame + 'FF' * 79)

            CODE_T_QUEUE.put(frame_bytes)

            # insert instruction information into sql
            parms = datetime.now(), instruction["inst_name"], instruction["inst_key"], \
                    instruction["inst_from"], instruction["inst_to"], instruction["inst_status"]
            insert_instruction(db, Instruction, *parms)
        except KeyError:
            print('<解译建链指令参数解析异常！>')
            # code:
            return response_wrapper({"code": 20000,
                                     "data": {"title": "指令发送提示", "message": "指令参数解析错误！", "type": "error"}})

        else:
            return response_wrapper({"code": 20000}, 'POST')


@coding.route('/uploadXml', methods=['POST', 'OPTIONS'])
@cross_origin()
def upload_xml():
    """this function used to save the template information VUE"""
    # if request.method == 'OPTIONS':
    #     return response_wrapper((None, 200), 'OPTIONS')

    if request.method == 'POST':
        xmls = request.files.getlist('xml')
        for file in xmls:
            filename = secure_filename(random_filename(file.filename))
            if filename == '':
                print('file upload failed!')
                continue
            if filename.endswith('.xml'):
                file.save(os.path.join(r'./static/xml_file/upload', filename))
                insert_sql_one_by_one(os.path.join(r'./static/xml_file/upload', filename))
            else:
                print('upload file {} is not xml file!'.format(filename))
        # return response_wrapper('success', 'post')
        return "success"


def insert_sql_one_by_one(file_path: str):
    try:
        if os.path.exists(file_path):
            xml = ParseXML(file_path).xml_info
            sql = Sql()
            sql.insert_decoding_data(sql.command, xml)

    except Exception as e:
        raise e
    else:
        print("upload xml files insert into sql sueecss!")

@coding.route('/uploadXmlLog', methods=["post"])
def upload_xml_write_sql():
    try:
        instruction = json.loads(request.get_data())
        # insert instruction information into sql

        parms = datetime.now(), instruction["inst_name"], instruction["inst_key"], \
                instruction["inst_from"], instruction["inst_to"], instruction["inst_status"]
        insert_instruction(db, Instruction, *parms)
    except:
        print("<upload xml insert sql error!>")
    else:
        return response_wrapper({"code": 20000}, 'POST')


@coding.route('/exportXml', methods=['POST', 'options'])
def export_xml():
    # if request.method == 'GET':
    #     sql = Sql()
    #     sql.search(sql.command, '2020-03-18 20:52:35', '2020-03-18 20:52:40')
    #     xml = WriteXML(sql.encoding_xml_array)
    #     xml.write_xml_file()
    """this function used to save the template information VUE"""
    if request.method == 'OPTIONS':
        return response_wrapper((None, 200), 'OPTIONS')

    if request.method == 'POST':
        res = json.loads(request.get_data())
        try:
            form = res["form"]
            instruction = res["instruction"]
            start_time = form['start_time']
            stop_time = form['stop_time']
        except KeyError:
            print('export_xml function has key error!')
        else:
            sql = Sql()
            sql.search(sql.command, start_time, stop_time)
            xml = WriteXML(sql.deconding_xml_array)
            xml.write_xml_file()

            zip_file = os.path.join(r'./static/zip_file/', xml.xml_file_name.split('.')[0]+'.zip')
            if os.path.exists(zip_file):
                # insert instruction information into sql
                parms = datetime.now(), instruction["inst_name"], instruction["inst_key"], \
                        instruction["inst_from"], instruction["inst_to"], instruction["inst_status"]
                insert_instruction(db, Instruction, *parms)
                # return the abstract file's path to the client
                return response_wrapper({"data": {
                                                 "link": urljoin(LOCAL_XML_URL, xml.xml_file_name.split('.')[0]+'.zip')
                                             },
                                        "code": 20000}, 'post')
            else:
                return response_wrapper({"data": 'error', "code": 20000}, 'post')


@coding.route('getInstruction', methods=["get"])
def get_instruction():
    """用于利用分页查询得方式获取指令的日志信息"""
    page_number = int(request.args.get("pageNumber"))
    page_size = int(request.args.get("pageSize"))

    total, items = query_instruction(Instruction, page_number, page_size)

    return response_wrapper({"code": 20000, "data": {"item": items, "total": total}}, 'get')

