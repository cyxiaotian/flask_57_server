#!/usr/bin/env python3
# -*- encoding:utf-8 -*-
# Author:caoy
# @Time:  14:06

# from bytesunpack import bytes_to_int
import time
import os
import json
from interface import IO_Operate, File_Info, parsed_spectrum_queue, spectrum_static_info
import uuid
from math import radians, cos, sin, asin, sqrt
import re


def geodistance(lng1,lat1,lng2,lat2):
    """公式计算两点间距离（km）"""
    #lng1,lat1,lng2,lat2 = (120.12802999999997,30.28708,115.86572000000001,28.7427)
    lng1, lat1, lng2, lat2 = map(radians, [float(lng1), float(lat1), float(lng2), float(lat2)]) # 经纬度转换成弧度
    dlon = lng2-lng1
    dlat = lat2-lat1
    a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
    distance = 2*asin(sqrt(a))*6371*1000 # 地球平均半径，6371km
    distance = round(distance/1000, 3)
    return distance


def fun(item, item2):
    return [item, item2]



def _geodistance(item1: tuple, item2: dict):
    """公式计算两点间距离（km）"""
    #lng1,lat1,lng2,lat2 = (120.12802999999997,30.28708,115.86572000000001,28.7427)

    lng1 = item1[0]
    lat1 = item1[1]

    lat2 = re.search('\d+(\.\d+)?', item2['中心纬度']).group()
    lng2 = re.search('\d+(\.\d+)?', item2['中心经度']).group()

    lng1, lat1, lng2, lat2 = map(radians, [float(lng1), float(lat1), float(lng2), float(lat2)]) # 经纬度转换成弧度
    dlon = lng2-lng1
    dlat = lat2-lat1
    a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
    distance = 2*asin(sqrt(a))*6371*1000 # 地球平均半径，6371km
    distance = round(distance/1000, 3)
    if distance < 2000:
        item2['点波束属性'] = "local_beam"
    else:
        item2['点波束属性'] = "far_beam"
    return item2


def random_filename(filename):
    ext = os.path.splitext(filename)[1]
    new_filename = uuid.uuid4().hex + ext
    return new_filename


def arr_size(arr, size):
    s = []
    for i in range(0, int(len(arr)) + 1, size):
        c = arr[i:i + size]
        if c != []:
            s.append(c)

    return s


def check_and_list_dir(fold_path: str, endwith: str)->list:
    """check the fold path and list all files endswith the endwith sign"""
    if os.path.isdir(fold_path):
        files = os.listdir(fold_path)
        return [os.path.join(fold_path, _) for _ in files if _.lower().endswith(endwith)]



# def sliding_window(source, data_len, byte_len, resolution=3000, mode='averge'):
#     '''
#     : 实时数据滑动窗口，计算 平均值，最大值，最小值
#     :param source: 数据源，默认数据，按照一整帧作为输入
#     :param data_len: 有效频谱数据长度
#     :param byte_len: 需要转换为int类型byte的字长
#     :param resolution: 分辨率
#     :param mode: 默认为 'average'
#     :return:
#     '''
#
#     win_size = data_len / resolution    # 窗口大小
#     index = 0                           # index 为滑动窗口计数
#     win_array = []                      # 窗口
#     sample_array = []                   # 抽样数组
#
#     half_index = int(source / byte_len)
#     if int(win_size) > half_index:      # 窗口大于源数据， 不需要抽样
#         return source
#     else:       # 窗口小于源数据, 需要抽样
#         for i in range(half_index):
#             if i % win_size == 0:
#                 # 计算采样平均值
#                 if 'average' == mode.lower():
#                     sample_array.append(int(sum(win_array) / win_size))
#                 elif 'max' == mode.lower():
#                     sample_array.append(int(max(win_array)))
#                 elif 'min' == mode.lower():
#                     sample_array.append(int(min(win_array)))
#                 # win_size 倍数处，进行填充该倍数处的 source value
#                 # win_size = []
#                 win_array = [source[i]]
#
#             else:
#                 win_array.append(source[i])
#
#         return sample_array         # 抽样后数据

def generater_file_name():
    return time.strftime("%Y%m%d_%H%M%S", time.localtime())


def sliding_window(source, data_len, resolution=3000, mode='average'):
    '''
    : 实时数据滑动窗口，计算 平均值，最大值，最小值
    :param source: 数据源，默认数据，按照一整帧作为输入
    :param data_len: 有效频谱数据长度
    :param byte_len: 需要转换为int类型byte的字长
    :param resolution: 分辨率
    :param mode: 默认为 'average'
    :return:
    '''

    win_size = data_len // resolution + 1   # 窗口大小
    win_array = []                      # 窗口
    # win_sum = 0
    sample_array = []                   # 抽样数组
    last_period = 0

    if int(resolution) > data_len:      # 窗口大于源数据， 不需要抽样
        return source
    else:       # 窗口小于源数据, 需要抽样
        try:
            for i in range(data_len-1):
                if (i+1) % win_size == 0:
                    # 计算采样平均值
                    win_array.append(source[i])
                    # win_sum += source[i]
                    if 'average' == mode.lower():
                        sample_array.append(int(sum(win_array) / win_size))
                    elif 'max' == mode.lower():
                        sample_array.append(int(max(win_array)))
                    elif 'min' == mode.lower():
                        sample_array.append(int(min(win_array)))
                    # win_size 倍数处，进行填充该倍数处的 source value
                    win_array = []
                    last_period = 0

                else:
                    last_period += 1
                    win_array.append(source[i])
            if last_period:     # not zero
                # 增加对最后一组不满窗口大小数据的统计
                sample_array.append(int(sum(win_array)) / last_period)

        except IndexError:
            print("IndexError!")
        return sample_array         # 抽样后数据


def array_splice_by_threshold(array: list, th_from: float, th_to: float):
    """
    :提取列表段
    :param array:
    :param th_from:
    :param th_to:
    :return:
    """
    result = list(filter(lambda x: x >= th_from and x <= th_to, array))
    if result:
        return result, array.index(result[0]), array.index(result[-1])
    else:
        return [], 0, 0


def read_json_file(fold_path: str, file_name: str):
    """read the json file under the fold"""
    file = os.path.join(fold_path, file_name)
    if os.path.exists(file):
        try:
            with open(file, 'r', encoding='utf-8') as fp:
                fp_json = json.load(fp)
                return fp_json
        except Exception as e:
            print('<json file load error!-{}>'.format(e))
    else:
        print('<read the file({}) not exists!>'.format(file))


def write_json_file(content, file_name: str = 'profile.cfg', fold_path: str = r'./static/config/profile'):
    """
    :write the json file under the fold
    :param content:
    :param file_name:
    :param fold_path:
    :return:
    """

    # 判断是否存在当前路径
    if os.path.exists(fold_path):
        # 存在当前 fold_path
        with open(os.path.join(fold_path, file_name), 'w', encoding='utf-8') as f_json:
            json.dump(content, f_json, ensure_ascii=False)
    else:
        # 不存在当前 user id, 创建
        os.mkdir(fold_path)
        with open(os.path.join(fold_path, file_name), 'w', encoding='utf-8') as f_json:
            json.dump(content, f_json, ensure_ascii=False)


def check_file_status(file: str, counter: int = 5):
    """
    : check the current's file status. over 5 seconds means written down.
    :param file: file full path
    :return: true is written down false is writing.
    """
    return (time.time() - os.path.getctime(file)) > counter




# data = b'\x00\x124V\x00x\x90\xab\x00\xcd\xef\x01\x00#\x004'
# result = []
# for i in range(int(len(data)/2)):
#     print(data[2*1:2*(i+1)])
#     result.append(bytes_to_int(data[2*i:2*(i+1)], 'big'))
#

if __name__ == '__main__':
    while True:
        file = File_Info(r'D:\python_project\flask_57\flask_server\static\dat_file\sp.dat')
        io_repay = IO_Operate('rb', file.path)
        io_repay.real_time_run()
        while True:
            parse_souce_data = spectrum_queue.get()
            value = sliding_window(parse_souce_data, spectrum_static_info['spectrum_num'], 2)
            x_multiple = (spectrum_static_info['stop_spectrum'] - spectrum_static_info['start_spectrum'])/spectrum_static_info['spectrum_num']
            b=[[spectrum_static_info['start_spectrum']+x_multiple*x, value[x]] for x in range(len(value))]