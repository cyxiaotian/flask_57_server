#!/usr/bin/env python3
# -*- encoding:utf-8 -*-
# Author:caoy
# @Time:  16:41

import os

BASEDIR = os.path.abspath(os.path.dirname(__file__))


DIALECT = 'mysql'
DRIVER = 'pymysql'
USERNAME = 'root'
PASSWORD = 'CY~~19880102'
HOST = '127.0.0.1'
PORT = '3306'
DATABASE = 'first_test'


class Config(object):
    SECRET_KEY = os.urandom(25)

    @staticmethod
    def init_app(app):
        pass


class DevelopmentConfig(Config):
    # SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(BASEDIR, 'data.db')
    # SQLALCHEMY_DATABASE_URI = "{}+{}://{}:{}@{}:{}/{}?charset=UTF8MB4".formatDIALECT, DRIVER, USERNAME, PASSWORD, HOST, PORT, DATABASE)

    SQLALCHEMY_COMMIT_ON_TEARDOWN = True
    SQLALCHEMY_TRACK_MODIFICATIONS = False  # 跟踪修改


class ProductionConfig(Config):
    SQLALCHEMY_DATABASE_URI = "{}+{}://{}:{}@{}:{}/{}?charset=utf8".format(DIALECT, DRIVER, USERNAME, PASSWORD, HOST,
                                                                           PORT, DATABASE)

    SQLALCHEMY_COMMIT_ON_TEARDOWN = True
    SQLALCHEMY_TRACK_MODIFICATIONS = False  # 跟踪修改


config = {
    'dev_config': DevelopmentConfig,
    "prod_config": ProductionConfig
}
