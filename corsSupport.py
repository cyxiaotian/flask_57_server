#!/usr/bin/env python3
# -*- encoding:utf-8 -*-
# Author:caoy
# @Time:  11:51

from flask import make_response, jsonify, request


def response_wrapper(resp, method_type: str):
    # result_text = {"statusCode": 200,"message": "请求成功"}
    resp = make_response(jsonify(resp))
    resp.headers['Access-Control-Allow-Origin'] = request.environ['HTTP_REFERER']
    # resp.headers['Access-Control-Allow-Origin'] = request.headers.get('Origin', '*')
    # resp.headers["Access-Control-Allow-Origin"] = '*'
    # resp.headers['Access-Control-Allow-Credentials'] = 'true'
    resp.headers['Access-Control-Allow-Headers'] ='Origin,X-Requested-With,Content-Type,Accept,content-type,application/json,Authorization,x-token'
    resp.headers['Access-Control-Allow-Methods'] = 'GET,PUT,POST,DELETE,OPTIONS'
    # resp.headers['Access-Control-Allow-Methods'] = method_type.upper()
    return resp
