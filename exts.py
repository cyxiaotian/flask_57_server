#!/usr/bin/env python3
# -*- encoding:utf-8 -*-
# Author:caoy
# @Time:  16:40

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from config import config


db = SQLAlchemy()


def creat_app():
    app = Flask(__name__)
    app.config.from_object(config["prod_config"])
    db.init_app(app)
    return app
