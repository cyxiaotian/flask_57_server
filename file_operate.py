#!/usr/bin/env python3
# -*- encoding:utf-8 -*-
# Author:caoy
# @Time:  18:31

import time
import os

fp_write_spectrum = [None]
FILE_SIZE = {
        "spectrum": 100*1024**2,
        "demode_rp": 100*1024**2,
        "demode_yp": 100*1024**2,
        "decoding": 100*1024**2
    }


def generater_file_name():
    return time.strftime("%Y%m%d_%H%M%S", time.localtime())


def save_spectrum_frame(frame_queue, file_mode='wb+'):
    global fp_write_spectrum
    dir_path = r'./static/stored_spectrum/'
    if os.path.isdir(dir_path):

        fp_write_spectrum[0] = open(dir_path + generater_file_name() + '.dat', file_mode)
        while True:
            print("写文件中！！！！！！！！！！")
            try:
                frame = frame_queue.get(block=True)
            except Exception as e:
                print("写文件都文件队列完成！")
            try:
                fp_write_spectrum[0].write(frame)
                fp_write_spectrum[0].flush()
            except ValueError:
                # 文件handler 关闭 退出循环
                print("写文件操作结束！")
                break
        return -1


def save_file(data_id: str, frame_queue, config_queue, file_save: dict, file_size: int = -1,
              fold_path: str =r'./static/dat_file/spectrum', file_mode: str = 'wb+'):
    """

    :param data_id:
    :param frame_queue:
    :param save_flag:
    :param file_size:
    :param fold_path:
    :param file_mode:
    :return:
    """
    # fold_path =  r'./static/dat_file/spectrum'
    fold_path = fold_path
    if not os.path.exists(fold_path):
        os.mkdir(fold_path)

    while True:             # 为了满足 存-不存 之间的切换
        # config = config_queue.get(block=False)
        if not file_save["flag"][data_id]:
            try:
                frame_queue.get(block=False)
            except Exception:
                pass
            continue
        file = generater_file_name() + '.dat'
        with open(os.path.join(fold_path, file), file_mode, buffering=0) as fp:
            # while os.path.getsize(os.path.join(fold_path, file)) / 1024 < file_size or file_size == -1:
            while os.path.getsize(os.path.join(fold_path, file)) < file_save["size"][data_id] or file_save["size"][data_id] == -1:
                try:
                    config = config_queue.get(block=False)
                    # f = file_save       # get the original file save information
                    t = {}
                    for k in config["file_size"].keys():
                        # file_save["size"][k] = config["file_size"][k]
                        t.update({k: config["file_size"][k]})
                    file_save.update({"size": t})

                except KeyError:
                    print('<file size setting parse parameters error!>')
                except Exception:
                    pass

                try:
                    frame = frame_queue.get(block=False)  # block=False   不需要阻塞
                except Exception:
                    # print('<文件写入过程读取队列异常>-%s' % e)
                    pass
                else:
                    if file_save["flag"][data_id]:
                        fp.write(frame)
                        del frame
                    else:
                        del frame
                        break
                    print("-----文件缓存队列大小：%s-----" % frame_queue.qsize())
