#!/usr/bin/env python3
# -*- encoding:utf-8 -*-
# Author:caoy
# @Time:  15:52

import os
import time
from threading import Thread
from queue import Queue
from bytesunpack import word_to_int

source_data_queue = Queue(10000)
spectrum_queue = Queue(100000)
spectrum_static_info = {}
ext_spectrum_bytes = b''





class File_Info():
    def __init__(self, path):
        self.path = path
        self.file_size = self.get_size()

    def get_size(self):
        pass


class IO_Replay(File_Info):
    def __init__(self, mode, path, endian='little'):
        super(IO_Replay, self).__init__(path)
        self.mode = mode
        self.endian = endian

    def check_file(self):
        if os.path.isfile(self.path):
            return True
        else:
            return False
            print('{}:is not a valid file!'.format(self.path))

    def read_in_chunks(self, work_queue, chunk_size=32):
        """    Lazy function (generator) to read a file piece by piece.
        Default chunk size: 1M    You can set your own chunk size     """
        file_object = open(self.path, self.mode)
        while True:
            try:
                chunk_data = file_object.read(chunk_size)
            except Exception as e:
                print(e)
            try:
                if chunk_data:
                    work_queue.put(chunk_data)
            except Exception as e:
                print("源数据压队列异常：{}".format(e))
            if not chunk_data:
                print('文件解析完成！文件名：{}'.format(self.path))
                # break
                file_object.close()
                time.sleep(10)
                file_object = open(self.path, self.mode)

            # # yield chunk_data

    # def parse_file(self):
    #     if self.check_file():
    #         try:
    #             with open(self.path, self.mode) as fp_lines:
    #                 for fp_line in fp_lines:
    #                     return fp_line
    #             return False
    #         except Exception as e:
    #             print('parse_file has exceptioo!: {}'.format(e))
    #     else:
    #         print('解析文件异常！文件为：{}'.format(self.path))

    def parse_file_put_queue(self, work_queue):
        if self.check_file():
            try:
                try:
                    work_queue.put(self.read_in_chunks(self.path))
                except Exception as e:
                    print("源数据压队列异常：{}".format(e))
                # with open(self.path, self.mode) as fp_lines:
                    # for fp_line in fp_lines:
                    #     # return fp_line
                    #     try:
                    #         work_queue.put(fp_line)
                    #     except Exception as e:
                    #         print("源数据压队列异常：{}".format(e))
                print("文件{}解析完毕!".format(self.path))
            except Exception as e:
                print('parse_file has exceptioo!: {}'.format(e))
        else:
            print('解析文件异常！文件为：{}'.format(self.path))

    def header_info_process(self):
        pass

    # def put_queue(self, work_queue):
    #     """ 数据压入 队列 """
    #     while self.parse_file():
    #     # if self.parse_file():
    #         try:
    #             work_queue.put(self.parse_file())
    #         except Exception as e:
    #             print("源数据压队列异常：{}".format(e))
    #     print("file data 读取完毕！")

    def get_spectrum_data(self, work_queue, parse_data_queue):
        """
        :解析压入队列中数据

        :param work_queue:  原始数据 queue
        :param parse_data_queue:  解析后频谱数据queue
        :return:
        """

        spectrum_array = []
        spectrum_bytes = b''
        added_spectrum_bytes = b''   # 补充帧
        header_info_len = 32        # 字节头部长32 bytes
        header_mask = bytes.fromhex('00'*14)

        while True:
            try:
                spectrum_bytes = b''
                spectrum_bytes = work_queue.get(block=False)
                if len(spectrum_bytes) == 0:
                    continue
            except Exception as e:
                print('解析队列异常！{}'.format(e))
                # break
            # spectrum_array.append(work_queue.get(blcok=False))
            # while len(spectrum_bytes) > 0:

            if header_mask in spectrum_bytes:    # 数据头包含在当前bytes流中
                index = spectrum_bytes.find(header_mask)    # 开始起始索引
                # 帧头之前的数据压入解析数据队列中
                for i in range(index//2):
                    spectrum_array.append(word_to_int(spectrum_bytes[2*i : 2*(i+1)], self.endian))
                # 判断是否为满一帧解析后的频谱数据， 满则压入解析后的数据队列
                try:
                    if len(spectrum_array) == spectrum_static_info['spectrum_num']:
                        parse_data_queue.put(spectrum_array[:])
                        spectrum_array.clear()
                except KeyError:
                    spectrum_array.clear()


                #判断帧头位置
                if len(spectrum_bytes) - index >= header_info_len:  # 所有数据头都包含
                    spectrum_static_info['start_spectrum'] = word_to_int(spectrum_bytes[index + 14: index + 16], self.endian)
                    spectrum_static_info['stop_spectrum'] = word_to_int(spectrum_bytes[index + 16: index + 18], self.endian)
                    spectrum_static_info['spectrum_num'] = word_to_int(spectrum_bytes[index + 18 : index + 20], self.endian) * 1024
                    for i in range((len(spectrum_bytes) - header_info_len)//2):
                        spectrum_array.append(word_to_int(spectrum_bytes[2*i+32:2*(i+1)+32], self.endian))
                        # 判断是否为满一帧解析后的频谱数据， 满则压入解析后的数据队列
                        try:
                            if len(spectrum_array) == spectrum_static_info['spectrum_num']:
                                parse_data_queue.put(spectrum_array[:])
                                spectrum_array.clear()
                        except KeyError:
                            spectrum_array.clear()
                else:
                    added_spectrum_bytes += spectrum_bytes[index:]     # 剩余字节长不够32字节长

            else:    # 不包含头部内容
                # 默认为量帧数据量一定够用（补充帧而言）
                # 两种情况，1，补充上一帧   2，非补充帧，确实没有
                if len(added_spectrum_bytes) != 0:   # 补充帧情况
                    index = spectrum_bytes.find(header_mask)  # 开始起始索引--理论上应为0
                    spectrum_static_info['start_spectrum'] = word_to_int(added_spectrum_bytes[index + 14: index + 16], self.endian)
                    spectrum_static_info['stop_spectrum'] = word_to_int(added_spectrum_bytes[index + 16: index + 18], self.endian)
                    spectrum_static_info['spectrum_num'] = word_to_int(added_spectrum_bytes[index + 18: index + 20], self.endian) * 1024
                    added_spectrum_bytes = b''
                else:
                    for i in range(len(spectrum_bytes)//2):
                        spectrum_array.append(word_to_int(spectrum_bytes[2*i:2*(i+1)], self.endian))

                    # 判断是否为满一帧解析后的频谱数据， 满则压入解析后的数据队列
                        try:
                            if len(spectrum_array) == spectrum_static_info['spectrum_num']:
                                parse_data_queue.put(spectrum_array[:])
                                spectrum_array.clear()
                        except KeyError:
                            spectrum_array.clear()

    def run_replay(self):
        """
        ：开启 读取，解析
        :return:
        """
        producer = Thread(target=self.read_in_chunks, args=(source_data_queue,), daemon=True)
        custumer = Thread(target=self.get_spectrum_data, args=(source_data_queue, spectrum_queue), daemon=True)
        producer.start()
        custumer.start()

if __name__ == '__main__':
    file = File_Info(r'D:\python_project\flask_57\flask_server\static\dat_file\sp.dat')
    io_repay = IO_Replay('rb', file.path)
    io_repay.run_replay()