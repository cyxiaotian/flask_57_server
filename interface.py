#!/usr/bin/env python3
# -*- encoding:utf-8 -*-
# Author:caoy
# @Time:  15:52

import os
import time
from copy import deepcopy
from threading import Thread
from queue import Queue as TQueue
from bytesunpack import bytes_to_int, bytes_to_float
from multiprocessing import Queue as MQueue


INDEX_COUNT = 0

__QUEUE_SIZE_SPECTRUM = 10000
__QUEUE_SIZE_DECODING = 10000
__QUEUE_SIZE_DEMODULATE = 10000

# use to caching the original received spectrum bytes
pre_parsed_spectrum_frame_queue = TQueue(__QUEUE_SIZE_SPECTRUM)
parsed_spectrum_queue = TQueue(__QUEUE_SIZE_SPECTRUM)

# use to caching the original received decoding bytes
pre_parsed_decoding_frame_queue = TQueue(__QUEUE_SIZE_DECODING)
parsed_decoding_queue = MQueue(__QUEUE_SIZE_DECODING)

# use to caching the original received demodulated bytes
pre_saved_demodulate_frame_queue = MQueue(__QUEUE_SIZE_DEMODULATE)   # just use to store

# use to cacheing the original received demodulated bytes   (rp)
pre_parsed_demodulate_frame_queue = MQueue(__QUEUE_SIZE_DEMODULATE)
parsed_demodulate_queue = MQueue(__QUEUE_SIZE_DEMODULATE)


pre_writed_frame_queue = TQueue(__QUEUE_SIZE_SPECTRUM)        # 文件存储queue

spectrum_static_info = {}
ext_spectrum_bytes = b''


FIRST_FRAME_FLAG = False


class File_Info():
    def __init__(self, path):
        self.path = path
        self.file_size = self.get_size()

    def get_size(self):
        pass


class IO_Operate(File_Info):
    def __init__(self, path: str = r'', mode: str = 'rb',  endian='little', max_size=10000):
        super(IO_Operate, self).__init__(path)
        self.mode = mode
        self.endian = endian
        self.first_frame_flag = False
        self.io_bytes_queue = TQueue(max_size)          # different client use this to replay spectrum
        self.io_data_queue = TQueue(max_size)
        self.io_static_info = {}
        self.io_data_info = {"header_info": {}, "body_info": []}                  # header information & parsed frame data

        #######################

    def check_file(self):
        if os.path.isfile(self.path):
            return True
        else:
            return False
            print('{}:is not a valid file!'.format(self.path))

    def read_in_chunks(self, work_queue, chunk_size=1024*300):
        """    Lazy function (generator) to read a file piece by piece.
        Default chunk size: 1M    You can set your own chunk size     """
        file_object = open(self.path, self.mode)
        while True:
            try:
                chunk_data = file_object.read(chunk_size)
            except Exception as e:
                print(e)
            try:
                if chunk_data:
                    work_queue.put(chunk_data)
                    # print("read file is running!")
                    # todo here for test save
                    # pre_writed_frame_queue.put(chunk_data, block=False, timeouot=1)
            except Exception as e:
                print("源数据压队列异常：{}".format(e))
            if not chunk_data:
                print('文件解析完成！文件名：{}'.format(self.path))
                file_object.close()
                # break
                time.sleep(10)
                file_object = open(self.path, self.mode)


    def receive_via_tcp(self, frame, parse_queue, write_flag=False, file_queue=pre_writed_frame_queue):
        """
        : 接收实时数据，入解析队列以及写文件队列
        :param frame: source frame via tcp channel
        :param parse_queue: used to parse data
        :param write_flag: if writed file
        :param write_mode: file operation mode
        :param file_queue: used to write to file
        :return:
        """
        while frame:
            if write_flag:
                try:
                    file_queue.put(frame)
                except Exception as e:
                    print("写文件队列完成！")
            else:
                try:
                    parse_queue.put(frame)
                except Exception as e:
                    print("接收实时数据完成！")


    # def parse_file(self):
    #     if self.check_file():
    #         try:
    #             with open(self.path, self.mode) as fp_lines:
    #                 for fp_line in fp_lines:
    #                     return fp_line
    #             return False
    #         except Exception as e:
    #             print('parse_file has exceptioo!: {}'.format(e))
    #     else:
    #         print('解析文件异常！文件为：{}'.format(self.path))

    def parse_file_put_queue(self, work_queue):
        if self.check_file():
            try:
                with open(self.path, self.mode) as fp_lines:
                    for fp_line in fp_lines:
                        try:
                            work_queue.put(fp_line)
                        except Exception as e:
                            print("源数据压队列异常：{}".format(e))
                print("文件{}解析完毕!".format(self.path))
            except Exception as e:
                print('parse_file has exceptioo!: {}'.format(e))
        else:
            print('解析文件异常！文件为：{}'.format(self.path))

    def header_info_process(self):
        pass

    def put_queue(self, work_queue):
        """ 数据压入 队列 """
        while self.parse_file():
        # if self.parse_file():
            try:
                work_queue.put(self.parse_file())
            except Exception as e:
                print("源数据压队列异常：{}".format(e))
        print("file data 读取完毕！")

    def get_spectrum_data(self, soc_data_queue, parse_data_queue):
        """
        :解析压入队列中数据

        :param soc_data_queue:  原始数据 queue
        :param parse_data_queue:  解析后频谱数据queue
        :return:
        """

        spectrum_array = []
        spectrum_bytes = b''
        added_spectrum_bytes = b''   # 补充帧
        header_info_len = 32        # 字节头部长32 bytes
        header_mask = bytes.fromhex('00'*14)

        while True:
            try:
                spectrum_bytes = b''
                spectrum_bytes = soc_data_queue.get(block=False)
                if len(spectrum_bytes) == 0:
                    continue
            except Exception as e:
                if not spectrum_bytes:
                    print('<频谱源数据队列已取空！>')
                else:
                    print('<spectrum data soc_queue get operation error!- %s>' % e)
            # spectrum_array.append(soc_data_queue.get(blcok=False))
            # while len(spectrum_bytes) > 0:

            if header_mask in spectrum_bytes:    # 数据头包含在当前bytes流中
                index = spectrum_bytes.find(header_mask)    # 开始起始索引
                # 帧头之前的数据压入解析数据队列中
                for i in range(index//2):
                    spectrum_array.append(bytes_to_int(spectrum_bytes[2*i : 2*(i+1)], self.endian))
                # 判断是否为满一帧解析后的频谱数据， 满则压入解析后的数据队列
                try:
                    if len(spectrum_array) == spectrum_static_info['spectrum_num']:
                        parse_data_queue.put(spectrum_array[:])
                        spectrum_array.clear()
                except KeyError:
                    spectrum_array.clear()


                #判断帧头位置
                if len(spectrum_bytes) - index >= header_info_len:  # 所有数据头都包含
                    spectrum_static_info['start_spectrum'] = bytes_to_int(spectrum_bytes[index + 14: index + 16], self.endian)
                    spectrum_static_info['stop_spectrum'] = bytes_to_int(spectrum_bytes[index + 16: index + 18], self.endian)
                    spectrum_static_info['spectrum_num'] = bytes_to_int(spectrum_bytes[index + 18 : index + 20], self.endian) * 1024
                    for i in range((len(spectrum_bytes) - header_info_len)//2):
                        spectrum_array.append(bytes_to_int(spectrum_bytes[2*i+32:2*(i+1)+32], self.endian))
                        # 判断是否为满一帧解析后的频谱数据， 满则压入解析后的数据队列
                        try:
                            if len(spectrum_array) == spectrum_static_info['spectrum_num']:
                                parse_data_queue.put(spectrum_array[:])
                                spectrum_array.clear()
                        except KeyError:
                            spectrum_array.clear()
                else:
                    added_spectrum_bytes += spectrum_bytes[index:]     # 剩余字节长不够32字节长

            else:    # 不包含头部内容
                # 默认为量帧数据量一定够用（补充帧而言）
                # 两种情况，1，补充上一帧   2，非补充帧，确实没有
                if len(added_spectrum_bytes) != 0:   # 补充帧情况
                    index = spectrum_bytes.find(header_mask)  # 开始起始索引--理论上应为0
                    spectrum_static_info['start_spectrum'] = bytes_to_int(added_spectrum_bytes[index + 14: index + 16], self.endian)
                    spectrum_static_info['stop_spectrum'] = bytes_to_int(added_spectrum_bytes[index + 16: index + 18], self.endian)
                    spectrum_static_info['spectrum_num'] = bytes_to_int(added_spectrum_bytes[index + 18: index + 20], self.endian) * 1024
                    added_spectrum_bytes = b''
                else:
                    for i in range(len(spectrum_bytes)//2):
                        spectrum_array.append(bytes_to_int(spectrum_bytes[2*i:2*(i+1)], self.endian))

                    # 判断是否为满一帧解析后的频谱数据， 满则压入解析后的数据队列
                        try:
                            if len(spectrum_array) == spectrum_static_info['spectrum_num']:
                                parse_data_queue.put(spectrum_array[:])
                                spectrum_array.clear()
                        except KeyError:
                            spectrum_array.clear()


    def check_exist_header_mask(self, source, mask):
        """
        :检测是否包含hearder_mask
        :param source:
        :param mask:
        :return: bool   True: exist  False: no exist
        """
        return True if source.find(mask) != -1 else False


    def parse_soc_decoding_bytes_to_int_quque(self, data_info, soc_queue, des_queue):
        """
        :@ parse decoding data(soc bytes) to parsed queue
        :param data_info:
        :param soc_queue:
        :param des_queue:
        :return:
        """
        added_spectrum_bytes = [b'']  # 补充帧
        header_mask = bytes.fromhex('A9CEC4E9')
        while True:
            try:
                source_bytes = b''
                source_bytes = soc_queue.get(block=True)  # 为True等待
            except Exception as e:
                if not source_bytes:
                    print('<软判源数据队列已取空！>')
                else:
                    print('<demodulate data soc_queue get operation error!- %s>' % e)
            else:
                self.parse_decoding_data_for_one_frame(source_bytes, added_spectrum_bytes, header_mask, data_info,
                                                         des_queue)
                del source_bytes


    def parse_soc_demodulation_bytes_to_int_queue(self, data_info, soc_queue, des_queue):
        """
        : 解析 demodulate data(rp) soc_queue 内容至 des_queue中
        :param self:
        :param static_info: 用于存储全局静态信息
        :param source:
        :param soc_queue:
        :param des_queue:
        :return:
        """
        added_spectrum_bytes = [b'']   # 补充帧
        header_mask = bytes.fromhex('A9CEC4E9')
        while True:
            try:
                source_bytes = b''
                source_bytes = soc_queue.get(block=True)   # 为True等待
            except Exception as e:
                if not source_bytes:
                    print('<软判源数据队列已取空！>')
                else:
                    print('<demodulate data soc_queue get operation error!- %s>' % e)
            else:
                self.parse_demodulate_data_for_one_frame(source_bytes, added_spectrum_bytes, header_mask, data_info, des_queue)
                del source_bytes

    def parse_soc_spectrum_bytes_to_int_queue(self, data_info, soc_queue, des_queue):
        """
        : 解析 soc_queue 内容至 des_queue中
        :param self:
        :param static_info: 用于存储全局静态信息
        :param source:
        :param soc_queue:
        :param des_queue:
        :return:
        """
        spectrum_array = []
        spectrum_bytes = b''
        added_spectrum_bytes = [b'']   # 补充帧
        # header_info_len = 32        # 字节头部长32 bytes
        header_mask = bytes.fromhex('A9CEC4E9')
        while True:
            try:
                source_bytes = b''
                source_bytes = soc_queue.get(block=True)   # 为True等待, False： 为空则 cache 到异常
            except Exception as e:
                if not source_bytes:
                    # print('<频谱源数据队列已取空！>')
                    pass
                else:
                    print('<spectrum data soc queue get operation error!>-%s' % e)
            # self.parse_spectrum_data_for_one_frame(source_bytes, added_spectrum_bytes, header_mask, static_info, spectrum_array, des_queue)
            else:
                self.parse_spectrum_data_for_one_frame(source_bytes, added_spectrum_bytes, header_mask, data_info, des_queue)
                del source_bytes


    def parse_decoding_data_for_one_frame(self, parsing_frame, added_frame, header_mask, data_info, des_queue):
        """
        :@ use to parsing decoding data(for one frame)
        :param parsing_frame:
        :param added_frame:
        :param header_mask:
        :param data_info:
        :param des_queue:
        :return:
        """

        multiple = 1 / 256.0
        header_info_len = 128       # 字节头部长 128 bytes

        if self.check_exist_header_mask(added_frame[0] + parsing_frame, header_mask):  # 当前帧里存在 header_mask
            # todo here 修改对首先帧的判断
            self.first_frame_flag = True

            if (added_frame[0]+parsing_frame).find(header_mask) == 0:  # 所有帧的第一帧
                pre_frame = added_frame[0]+parsing_frame
                added_frame[0] = b''
                last_byte = b'' if len(pre_frame) % 2 == 0 else pre_frame[-1:]      # 奇偶帧最后一个字节的处理
                self._parse_header_info(pre_frame, 0, data_info['header_info'])     # 此处0代表header在首位0索引
                current_frame = pre_frame[header_info_len:]
                # 解析帧头后的帧数据，并对入队列以及拼帧进行处理
                cuted_bytes = b''
                for i in range(len(current_frame) // 2):
                    data_info["body_info"].append(
                        bytes_to_int(current_frame[2 * i:2 * (i + 1)], self.endian) * multiple
                    )
                    if self._checked_and_put_queue(data_info, des_queue):
                        cuted_bytes = current_frame[2*(i+1):]
                        break
                # 如果入队列后并有剩余数据，则拼到下一帧里处理
                added_frame[0] += cuted_bytes + last_byte if len(cuted_bytes) == 0 else cuted_bytes

            else:  # added_frame[0] 有内容，为非第一帧(all)
                current_frame = added_frame[0] + parsing_frame  # 上一次剩余帧+当前帧组成拼接帧
                added_frame[0] = b''
                header_mask_index = current_frame.find(header_mask)  # header_mask 索引

                # 当前情况为：header_mask 为该帧中间完整出现
                last_byte = b'' if len(current_frame) % 2 == 0 else current_frame[-1:]  # 奇偶帧最后一个字节的处理
                # 解析帧头前的帧数据，并对入队列以及拼帧进行处理
                # 此部分默认每一帧都是正确的帧 没有异常情况
                # hearder mask之前的内容压入列表
                cuted_bytes = b''
                for i in range(header_mask_index // 2):     # header_mask 之前的内容
                    data_info["body_info"].append(
                        bytes_to_int(current_frame[2 * i: 2 * (i + 1)], self.endian) * multiple
                    )
                    if self._checked_and_put_queue(data_info, des_queue):
                        # 解析完一帧后，从帧头处截断拼下一帧
                        cuted_bytes = current_frame[header_mask_index:]
                        break
                added_frame[0] += cuted_bytes + last_byte if len(cuted_bytes) == 0 else cuted_bytes

        else:  # 当前帧里没有找到帧头
            if self.first_frame_flag:
                current_frame = added_frame[0] + parsing_frame  # 上一次剩余帧+当前帧组成拼接帧
                added_frame[0] = b''
                last_byte = b'' if len(current_frame) % 2 == 0 else current_frame[-1:]  # 奇偶帧最后一个字节的处理
                # 解析帧头后的帧数据，并对入队列以及拼帧进行处理
                cuted_bytes = b''
                for i in range(len(current_frame) // 2):
                    data_info["body_info"].append(
                        bytes_to_int(current_frame[2 * i: 2 * (i + 1)], self.endian) * multiple
                    )
                    if self._checked_and_put_queue(data_info, des_queue):
                        cuted_bytes = current_frame[2*(i+1):]
                        break
                # 如果入队列后并有剩余数据，则拼到下一帧里处理
                added_frame[0] += cuted_bytes + last_byte if len(cuted_bytes) == 0 else cuted_bytes

        """ spectrum data one frame process """


    def parse_demodulate_data_for_one_frame(self, parsing_frame, added_frame, header_mask, data_info, des_queue):
        """

        :param parsing_frame:
        :param added_frame:
        :param header_mask:
        :param data_info:
        :param des_queue:
        :return:
        """

        multiple = 1 / 256.0
        header_info_len = 128  # 字节头部长 128 bytes

        if self.check_exist_header_mask(added_frame[0] + parsing_frame, header_mask):  # 当前帧里存在 header_mask
            # todo here 修改对首先帧的判断
            self.first_frame_flag = True

            if (added_frame[0] + parsing_frame).find(header_mask) == 0:  # 所有帧的第一帧
                pre_frame = added_frame[0] + parsing_frame
                added_frame[0] = b''
                last_byte = b'' if len(pre_frame) % 2 == 0 else pre_frame[-1:]  # 奇偶帧最后一个字节的处理
                self._parse_header_info(pre_frame, 0, data_info['header_info'])  # 此处0代表header在首位0索引
                current_frame = pre_frame[header_info_len:]
                # 解析帧头后的帧数据，并对入队列以及拼帧进行处理
                cuted_bytes = b''
                for i in range(len(current_frame) // 2):
                    data_info["body_info"].append(
                        bytes_to_int(current_frame[2 * i:2 * (i + 1)], self.endian) * multiple
                    )
                    if self._checked_and_put_queue(data_info, des_queue):
                        cuted_bytes = current_frame[2 * (i + 1):]
                        break
                # 如果入队列后并有剩余数据，则拼到下一帧里处理
                added_frame[0] += cuted_bytes + last_byte if len(cuted_bytes) == 0 else cuted_bytes

            else:  # added_frame[0] 有内容，为非第一帧(all)
                current_frame = added_frame[0] + parsing_frame  # 上一次剩余帧+当前帧组成拼接帧
                added_frame[0] = b''
                header_mask_index = current_frame.find(header_mask)  # header_mask 索引

                # 当前情况为：header_mask 为该帧中间完整出现
                last_byte = b'' if len(current_frame) % 2 == 0 else current_frame[-1:]  # 奇偶帧最后一个字节的处理
                # 解析帧头前的帧数据，并对入队列以及拼帧进行处理
                # 此部分默认每一帧都是正确的帧 没有异常情况
                # hearder mask之前的内容压入列表
                cuted_bytes = b''
                for i in range(header_mask_index // 2):  # header_mask 之前的内容
                    data_info["body_info"].append(
                        bytes_to_int(current_frame[2 * i: 2 * (i + 1)], self.endian) * multiple
                    )
                    if self._checked_and_put_queue(data_info, des_queue):
                        # 解析完一帧后，从帧头处截断拼下一帧
                        cuted_bytes = current_frame[header_mask_index:]
                        break
                added_frame[0] += cuted_bytes + last_byte if len(cuted_bytes) == 0 else cuted_bytes

        else:  # 当前帧里没有找到帧头
            if self.first_frame_flag:
                current_frame = added_frame[0] + parsing_frame  # 上一次剩余帧+当前帧组成拼接帧
                added_frame[0] = b''
                last_byte = b'' if len(current_frame) % 2 == 0 else current_frame[-1:]  # 奇偶帧最后一个字节的处理
                # 解析帧头后的帧数据，并对入队列以及拼帧进行处理
                cuted_bytes = b''
                for i in range(len(current_frame) // 2):
                    data_info["body_info"].append(
                        bytes_to_int(current_frame[2 * i: 2 * (i + 1)], self.endian) * multiple
                    )
                    if self._checked_and_put_queue(data_info, des_queue):
                        cuted_bytes = current_frame[2 * (i + 1):]
                        break
                # 如果入队列后并有剩余数据，则拼到下一帧里处理
                added_frame[0] += cuted_bytes + last_byte if len(cuted_bytes) == 0 else cuted_bytes

        """ spectrum data one frame process """


    def parse_spectrum_data_for_one_frame(self, parsing_frame, added_frame, header_mask, data_info, des_queue):
        """

        :param self:
        :param parsing_frame: 当前帧
        :param header_mask: 帧头
        :param static_info: 全局静态信息
        :param count: spectrum_array append 计数
        :param des_queue:
        :return:
        """
        header_info_len = 128       # 字节头部长 128 bytes
        if self.check_exist_header_mask(added_frame[0] + parsing_frame, header_mask):  # 当前帧里存在 header_mask
            # todo here 修改对首先帧的判断
            self.first_frame_flag = True
            # if len(added_frame[0]) == 0 and (added_frame[0]+parsing_frame).find(header_mask) == 0:  # 所有帧的第一帧
            if (added_frame[0]+parsing_frame).find(header_mask) == 0:  # 所有帧的第一帧
                pre_frame = added_frame[0]+parsing_frame
                added_frame[0] = b''
                # last_byte = b'' if len(parsing_frame) % 2 == 0 else parsing_frame[-1:]  # 奇偶帧最后一个字节的处理
                last_byte = b'' if len(pre_frame) % 2 == 0 else pre_frame[-1:]      # 奇偶帧最后一个字节的处理
                self._parse_header_info(pre_frame, 0, data_info['header_info'])     # 此处0代表header在首位0索引
                # useful_data_len = data_info["header_info"]['frame_len']
                current_frame = pre_frame[header_info_len:]
                # 解析帧头后的帧数据，并对入队列以及拼帧进行处理
                cuted_bytes = b''
                for i in range(len(current_frame) // 2):
                    data_info["body_info"].append(bytes_to_int(current_frame[2 * i:2 * (i + 1)], self.endian))
                    if self._checked_and_put_queue(data_info, des_queue):
                        cuted_bytes = current_frame[2*(i+1):]
                        break
                # 如果入队列后并有剩余数据，则拼到下一帧里处理
                added_frame[0] += cuted_bytes + last_byte if len(cuted_bytes) == 0 else cuted_bytes

            else:  # added_frame[0] 有内容，为非第一帧(all)
                current_frame = added_frame[0] + parsing_frame  # 上一次剩余帧+当前帧组成拼接帧
                added_frame[0] = b''
                header_mask_index = current_frame.find(header_mask)  # header_mask 索引

                # 当前情况为：header_mask 为该帧中间完整出现
                last_byte = b'' if len(current_frame) % 2 == 0 else current_frame[-1:]  # 奇偶帧最后一个字节的处理
                # for i in range(len(current_frame) // 2):
                # 解析帧头前的帧数据，并对入队列以及拼帧进行处理
                # 此部分默认每一帧都是正确的帧 没有异常情况
                # hearder mask之前的内容压入列表
                cuted_bytes = b''
                for i in range(header_mask_index // 2):     # header_mask 之前的内容
                    data_info["body_info"].append(bytes_to_int(current_frame[2 * i: 2 * (i + 1)], self.endian))
                    if self._checked_and_put_queue(data_info, des_queue):
                        # 解析完一帧后，从帧头处截断拼下一帧
                        cuted_bytes = current_frame[header_mask_index:]
                        break
                added_frame[0] += cuted_bytes + last_byte if len(cuted_bytes) == 0 else cuted_bytes

        else:  # 当前帧里没有找到帧头
            if self.first_frame_flag:
                current_frame = added_frame[0] + parsing_frame  # 上一次剩余帧+当前帧组成拼接帧
                added_frame[0] = b''
                last_byte = b'' if len(current_frame) % 2 == 0 else current_frame[-1:]  # 奇偶帧最后一个字节的处理
                # 解析帧头后的帧数据，并对入队列以及拼帧进行处理
                cuted_bytes = b''
                for i in range(len(current_frame) // 2):
                    data_info["body_info"].append(bytes_to_int(current_frame[2 * i: 2 * (i + 1)], self.endian))
                    if self._checked_and_put_queue(data_info, des_queue):
                        cuted_bytes = current_frame[2*(i+1):]
                        break
                # 如果入队列后并有剩余数据，则拼到下一帧里处理
                added_frame[0] += cuted_bytes + last_byte if len(cuted_bytes) == 0 else cuted_bytes

    def parse_header_info(self, parsing_frame, header_index, static_info):
        """
        : 32 bytes header parsed
        :param self:
        :param parsing_frame: 当前解析帧
        :param header_index: 头指针首地址
        :param static_info:  全局静态信息
        :return:
        """
        static_info['start_spectrum'] = bytes_to_int(parsing_frame[header_index + 14: header_index + 16], self.endian)
        static_info['stop_spectrum'] = bytes_to_int(parsing_frame[header_index + 16: header_index + 18], self.endian)
        static_info['spectrum_num'] = bytes_to_int(parsing_frame[header_index + 18: header_index + 20],
                                                  self.endian) * 1024

    def __parse_timestamp(self, frame: bytes, static_info: dict):
        """时间戳解析  12bytes"""
        # 4 bytes 毫秒值
        static_info['microsecond'] = bytes_to_int(frame[8:], self.endian)
        # 年月日 时分秒
        static_info['timestamp'] = str(bytes_to_int(frame[6:8], self.endian)).zfill(4) + '-' + \
                                   str(frame[5]).zfill(2) + '-' + \
                                   str(frame[4]).zfill(2) + ' ' + \
                                   str(frame[3]).zfill(2) + ':' + \
                                   str(frame[2]).zfill(2) + ':' + \
                                   str(frame[1]).zfill(2)

    def _parse_header_info(self, frame, header_index, static_info):
        """
        :128 字节通用拆头
        :param parsing_frame:
        :param header_index:
        :param static_info:
        :return:
        """
        SYNC_HEAD = 4       # Synchronous Head length
        RESERVED_FOUR = 4

        # parse the timestamp
        self.__parse_timestamp(frame[8:20], static_info)
        # 经度
        static_info['lon'] = bytes_to_float(frame[header_index+20:header_index+24], self.endian)
        # 纬度
        static_info['lat'] = bytes_to_float(frame[header_index+24:header_index+28], self.endian)
        # 通道号
        static_info['channel'] = frame[34]
        # AD通道号
        one_byte = frame[35]
        if one_byte == 0x0:
            static_info['AD_channel'] = '硬件通道A'
        elif one_byte == 0x40:
            static_info['AD_channel'] = '硬件通道B'
        else:
            static_info['AD_channel'] = 'error'
        # 排序时间读取指示

        if one_byte & 0x30 == 0x0:
            static_info['AD_channel'] = 'WINDOW时间表示法'
        elif one_byte & 0xc0 == 0x10:
            static_info['AD_channel'] = 'GPS/北斗'
        elif one_byte & 0xc0 == 0x20:
            static_info['AD_channel'] = 'B码'
        elif one_byte & 0xc0 == 0x30:
            static_info['AD_channel'] = '脉冲计数器'
        else:
            static_info['AD_channel'] = 'error'
        # 射频频段 保留
        # 通道i中心频点 hz
        static_info['channel_i_spectrum'] = bytes_to_float(frame[header_index + 36:header_index + 40], self.endian)
        # 射频变频关系
        static_info['channel_i_spectrum'] = '18.25GHz' if frame[40] == 0 else '待定'
        # 采集输出数据类型
        one_byte = frame[42]
        if one_byte == int('a1', 16):
            static_info['sample_data_type'] = 'AD1采样基带DDC输出数据'
        elif one_byte == int('a2', 16):
            static_info['sample_data_type'] = 'AD2采样基带DDC输出数据'
        elif one_byte == int('d1', 16):
            static_info['sample_data_type'] = '通道1解调输出数据'
        elif one_byte == int('d2', 16):
            static_info['sample_data_type'] = '通道1译码输出数据'
        elif one_byte == int('d3', 16):
            static_info['sample_data_type'] = '通道2解调输出数据'
        elif one_byte == int('d4', 16):
            static_info['sample_data_type'] = '通道2译码输出数据'
        elif one_byte == int('e1', 16):
            static_info['sample_data_type'] = '通道1频谱输出数据'
        elif one_byte == int('e2', 16):
            static_info['sample_data_type'] = '通道2频谱输出数据'
        else:
            static_info['sample_data_type'] = 'error'

        # 增益
        static_info['gain'] = frame[43]
        # 能量值
        static_info['power'] = bytes_to_float(frame[header_index + 44:header_index + 48], self.endian)

        # 信令控制命令类型
        one_byte = frame[48]
        if one_byte == 0x00:
            static_info['signalling'] = '参数建链命令'
        elif one_byte == 0x01:
            static_info['signalling'] = '参数建链响应'
        elif one_byte == 0x02:
            static_info['signalling'] = '参数拆链命令'
        elif one_byte == 0x03:
            static_info['signalling'] = '参数拆链响应'
        else:
            static_info['signalling'] = 'error'

        # 数据类型
        one_byte = frame[53]

        if one_byte & 0x01 == 0x0:
            static_info['data_type'] = {'DDC': '不采'}
        else:
            static_info['data_type'] = {'DDC': '采集'}

        if one_byte & 0x02 == 0x0:
            static_info['data_type'].update({'接调': '不采'})
        else:
            static_info['data_type'].update({'接调': '采集'})

        if one_byte & 0x04 == 0x0:
            static_info['data_type'].update({'译码': '不采'})
        else:
            static_info['data_type'].update({'译码': '采集'})
        # 增益方式
        one_byte = frame[54]
        if one_byte == 0:
            static_info['gain_type'] = 'AGC'
        elif one_byte == 1:
            static_info['gain_type'] = 'MGC'
        else:
            static_info['gain_type'] = 'error'

        # 增益值
        static_info['gain_value'] = frame[55]
        # 采集数据大小 4 bytes front endian 填写
        # 采集数据时间 s
        static_info['collect_time'] = bytes_to_int(frame[header_index + 60:header_index + 62], self.endian)
        # 星类型
        one_byte = frame[63]
        if one_byte == 0x00:
            static_info['sat_lon'] = '62.6'
        elif one_byte == 0x01:
            static_info['sat_lon'] = '31.5'
        elif one_byte == 0x02:
            static_info['sat_lon'] = '117.5'
        elif one_byte == 0x03:
            static_info['sat_lon'] = '179.6'
        elif one_byte == 0x04:
            static_info['sat_lon'] = '-55.0'
        elif one_byte == 0x05:
            static_info['sat_lon'] = '-0.7'

        # 频谱起始频率
        static_info['start_spectrum'] = bytes_to_int(frame[header_index + 64:header_index + 66], self.endian)
        # 频谱结束频率
        static_info['stop_spectrum'] = bytes_to_int(frame[header_index + 66:header_index + 68], self.endian)
        # 协议版本号
        static_info['version'] = frame[67]
        # 子载波类型
        one_byte = frame[80]
        if one_byte == 0:
            static_info['subcarrier_type'] = '120K'
        elif one_byte == 1:
            static_info['subcarrier_type'] = 'DVB-S2'
        else:
            static_info['subcarrier_type'] = 'MF-TDMA'

        # 数据包标志(char)
        static_info['pkg_len'] = frame[81]

        # 帧流水号(short)
        static_info['pkg_len'] = bytes_to_int(frame[header_index + 82:header_index + 84], self.endian)

        # 有效帧长(int)
        static_info['frame_len'] = bytes_to_int(frame[header_index + 84:header_index + 88], self.endian)

        # 解调误码率（float)
        static_info['error_rate'] = bytes_to_float(frame[header_index + 88:header_index + 92], self.endian)

        # SNR（信噪比）值(float)
        static_info['snr'] = bytes_to_float(frame[header_index + 92:header_index + 96], self.endian)

        # 频偏(short)
        static_info['frequency_offset'] = bytes_to_int(frame[header_index + 100:header_index + 102], self.endian)
        # CRC校验标志
        one_byte = frame[104]
        if one_byte == 0x0:
            static_info['crc'] = 'CRC不通过'
        elif one_byte == 0x01:
            static_info['crc'] = 'CRC通过'
        elif one_byte == 0x0f:
            static_info['crc'] = '无CRC'
        else:
            static_info['crc'] = 'error'

    def _checked_and_put_queue(self, data: dict, queue):
        """
        :check the data and put the data into the queue
        :param data: io_data_info
        :return:
        """
        global INDEX_COUNT
        data_len = data["header_info"]["frame_len"]
        assert data_len
        # print(len(data["body_info"]))
        if len(data["body_info"]) == data_len//2:
            try:
                data["body_info"] = data["body_info"][:data_len]
                queue.put(deepcopy(data))
                # INDEX_COUNT += 1
                # print("解析第 %s 次 %s" % (INDEX_COUNT,sum(data["body_info"])))
                return True
            except Exception as e:
                print('<queue put data has something wrong!-{}>'.format(e))
                return False
            finally:
                data["body_info"].clear()
                # print("取第 %s 次 %s" % (INDEX_COUNT, sum(queue.get()["body_info"])))

    def check_to_put_queue(self, array, size, queue):
        """
        : 满size提交array -> queue
        :param self:
        :param array: 承载提交数的数组
        :param size:  判读需要提交数组的大小阈值
        :param queue:
        :return:
        """
        assert size
        if len(array) == size:
            try:
                queue.put(array[:])
            except Exception as e:
                print('queue put array has something wrong!')
            array.clear()
            return True
        return False


    def real_time_decoding_run(self, targets, recv_queue: TQueue, parsed_queue: TQueue,
                                   save_queue:[TQueue, MQueue], send_queue: TQueue, recv_flag, save_flag):
        """
        :@ use to receive the decoding data and parsed the decoding data & save the data
        :param targets: the target use to receive the tcp data
        :param recv_queue: tcp receive queue(soc bytes's data)
        :param parsed_queue: the parsed data queue(des parsed data)
        :param save_queue: the saving data queue(soc bytes's data)
        :param send_queue: the send data queue(bytes's data send back to tcp server)
        :param recv_flag: if need to receive tcp data(soc bytes's data)
        :param save_flag: if need to save the received data(soc bytes's data)
        :return:
        """
        producer = Thread(target=targets,
                          args=(recv_queue, save_queue, send_queue, recv_flag, save_flag),
                          daemon=True)

        custumer = Thread(target=self.parse_soc_decoding_bytes_to_int_quque,
                          args=(self.io_data_info, recv_queue, parsed_queue),
                          daemon=True)

        producer.start()
        custumer.start()



    # multiprocessing
    # def real_time_demodulation_run(self, targets, recv_queue: MQueue = pre_parsed_demodulate_frame_queue, parsed_queue: MQueue = parsed_demodulate_queue):
    #     """
    #     : real time demodulate. recv bytes from the tcp server and parsed it
    #     : mutiprocessing
    #     :param targets: tcp client translate_data
    #     :param recv_queue: soc bytes data queue(multiprocess queue)
    #     :return:
    #     """
    #     producer = Thread(target=targets, args=(recv_queue,), daemon=True)
    #     custumer = Thread(target=self.parse_soc_demodulation_bytes_to_int_queue,
    #                       args=(self.io_data_info, recv_queue, parsed_queue))
    #
    #     producer.start()
    #     custumer.start()

    def real_time_demodulation_run(self, targets, recv_queue: TQueue, parsed_queue: TQueue,
                                   save_queue: [TQueue, MQueue], send_queue: TQueue, parse_flag, save_flag):
        """
        ： recv bytes from the tcp server and parsed it
        :param target:  tcp client translate_data
        :param recv_queue: soc bytes data queue
        :param parsed_quee: the parsed data queue
        :param save_queue: the save data queue
        :param send_queue: used to send data to tcp server
        :param parse_flag: parse data identify
        :param save_flag: save date identify
        :return:
        """
        producer = Thread(target=targets,
                          args=(recv_queue, save_queue, send_queue, parse_flag, save_flag),
                          daemon=True)

        customer = Thread(target=self.parse_soc_demodulation_bytes_to_int_queue,
                          args=(self.io_data_info, recv_queue, parsed_queue),
                          daemon=True)

        producer.start()
        customer.start()

    def real_time_spectrum_run(self, targets, recv_queue: TQueue, save_queue: [TQueue, MQueue], send_queue: TQueue, parse_flag, save_flag):
        """
        ： recv bytes from the tcp server and parsed it
        :param target:  tcp client translate_data
        :param recv_queue: soc bytes data queue
        :param save_queue: save data queue
        :param send_queue: used to send data to tcp server
        :param parse_flag: parse data identify
        :param save_flag: save date identify
        :return:
        """
        producer = Thread(target=targets,
                          args=(recv_queue, save_queue, send_queue, parse_flag, save_flag),
                          daemon=True)

        customer = Thread(target=self.parse_soc_spectrum_bytes_to_int_queue,
                          args=(self.io_data_info, self.io_bytes_queue, self.io_data_queue),
                          daemon=True)

        producer.start()
        customer.start()

    '''  2020-04-27
        producer = Thread(target=self.read_in_chunks, args=(pre_parsed_spectrum_frame_queue,), daemon=True)
        # producer = Thread(target=self.parse_file_put_queue, args=(pre_parsed_spectrum_frame_queue,), daemon=True)
        # custumer = Thread(target=self.get_spectrum_data, args=(pre_parsed_spectrum_frame_queue, parsed_spectrum_queue), daemon=True)
        custumer = Thread(target=self.parse_soc_spectrum_bytes_to_int_queue,
                          args=(spectrum_static_info, pre_parsed_spectrum_frame_queue, parsed_spectrum_queue), daemon=True)

        producer.start()
        custumer.start()
    '''

    def replay_time_spectrum_run(self):
        producer = Thread(target=self.read_in_chunks,
                          args=(self.io_bytes_queue,),
                          daemon=True)

        custumer = Thread(target=self.parse_soc_spectrum_bytes_to_int_queue,
                          args=(self.io_data_info, self.io_bytes_queue, self.io_data_queue),
                          daemon=True)
        producer.start()
        custumer.start()


if __name__ == '__main__':
    file = File_Info(r'F:\docx\57所\version2\SP.dat')
    io_repay = IO_Operate(file.path, 'rb')
    # io_repay.real_time_run()
    io_repay.replay_time_spectrum_run()
    print("success down")