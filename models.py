#!/usr/bin/env python3
# -*- encoding:utf-8 -*-
# Author:caoy
# @Time:  16:57

from exts import db


class Files(db.Model):
    """用于mapping文件存储模型"""
    __tablename__ = "files"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    spectrum_size = db.Column(db.Integer, nullable=False)
    demode_rp_size = db.Column(db.Integer, nullable=False)
    demode_yp_size = db.Column(db.Integer, nullable=False)
    decoding_size = db.Column(db.Integer, nullable=False)

    spectrum_flag = db.Column(db.Boolean, nullable=False)
    demode_rp_flag = db.Column(db.Boolean, nullable=False)
    demode_yp_flag = db.Column(db.Boolean, nullable=False)
    decoding_flag = db.Column(db.Boolean, nullable=False)

    def __init__(self, spectrum_size, demode_rp_size, demode_yp_size, decoding_size,
                 spectrum_flag, demode_rp_flag, demode_yp_flag, decoding_flag):
        self.spectrum_size = spectrum_size
        self.demode_rp_size = demode_rp_size
        self.demode_yp_size = demode_yp_size
        self.decoding_size = decoding_size

        self.spectrum_flag = spectrum_flag
        self.demode_rp_flag = demode_rp_flag
        self.demode_yp_flag = demode_yp_flag
        self.decoding_flag = decoding_flag


class Instruction(db.Model):
    __tablename__ = "instruction"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    inst_date = db.Column(db.DateTime, nullable=False)
    inst_name = db.Column(db.String(15), nullable=False)
    inst_key = db.Column(db.String(10), nullable=False)
    inst_from = db.Column(db.String(25), nullable=False)
    inst_to = db.Column(db.String(25), nullable=False)
    inst_status = db.Column(db.String(5), nullable=False)

    def __init__(self, inst_date, inst_name, inst_key, inst_from, inst_to, inst_status):
        self.inst_date = inst_date
        self.inst_name = inst_name
        self.inst_key = inst_key
        self.inst_from = inst_from
        self.inst_to = inst_to
        self.inst_status = inst_status
