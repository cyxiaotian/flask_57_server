
from setting import setting, request
from corsSupport import response_wrapper
import json
from utils.variable import PROFILE_CONFIG_M_QUEUE
from exts import db
from models import Files
from sql_operation.orm import update_file, query_file


@setting.route('/setFileSize', methods=['options', "post"])
def file_size_setting():
    if request.method == 'OPTIONS':
        return response_wrapper((200, None), 'options')

    if request.method == "POST":
        unit = 1024**2     # MB
        try:
            res = json.loads(request.get_data())
            v = {"file_size":
                     {
                         "spectrum": int(res["ppSize"]) * unit,
                         "demode_rp": int(res["rpSize"]) * unit,
                         "demode_yp": int(res["ypSize"]) * unit,
                         "decoding": int(res["ymSize"]) * unit
                     }
                 }
            PROFILE_CONFIG_M_QUEUE.put(v)
            # the next is writing the configure information into sql
            update_file(db,
                        Files,
                        {
                            "spectrum_size": v["file_size"]["spectrum"],
                            "demode_rp_size": v["file_size"]["demode_rp"],
                            "demode_yp_size": v["file_size"]["demode_yp"],
                            "decoding_size": v["file_size"]["decoding"]}
                        )

        except KeyError:
            print('<配置设置参数解析异常！>')
        # code:
            return response_wrapper({"code": 20000,
                                 "data": {"title": "指令发送提示", "message": "指令参数解析错误！", "type": "error"}}, 'post')

        except Exception as e:
            print("<文件大小设置提交数据库异常!-{}>".format(e))

        else:
            return response_wrapper({"code": 20000}, 'POST')


@setting.route('/getFileSize', methods=["get"])
def get_size_setting():
    r = query_file(Files)
    config = {"ppSize": r[4], "rpSize": r[5], "ypSize": r[6], "ymSize": r[7]}
    return response_wrapper({"code": 20000, "data": config}, "get")
