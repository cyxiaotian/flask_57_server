#!/usr/bin/env python3
# -*- encoding:utf-8 -*-
# Author:caoy
# @Time:  18:46


def insert_file(db, cls, *args):
    """插入文件信息"""
    _ = cls(*args)
    db.session.add(_)
    try:
        db.session.commit()
    except:
        db.session.rollback()


def query_file(cls):
    """查询文件信息"""
    try:
        r = cls.query.first()     # None means nothing
        return (r.spectrum_flag, r.demode_rp_flag, r.demode_yp_flag, r.decoding_flag,
            r.spectrum_size, r.demode_rp_size, r.demode_yp_size, r.decoding_size)
    except Exception as e:
        print('<文件信息查询发生异常！>')
        raise e


def update_file(db, cls, d: dict):
    """更新文件信息"""
    try:
        r = cls.query.filter_by(id=1)
        r.update(d)
        db.session.commit()
    except Exception as e:
        print(e)
        print('<文件信息数据表数据更新异常！>')


def insert_instruction(db, cls, *args):
    """插入指令信息"""
    _ = cls(*args)
    db.session.add(_)
    try:
        db.session.commit()
    except:
        db.session.rollback()


def query_instruction(cls, page_number: int, page_size: int):
    """分页查询指令日志"""
    date_formatter = "%Y-%m-%d %H:%M:%S"
    try:
        r = cls.query.order_by(cls.id.desc()).paginate(page=page_number, per_page=page_size, error_out=False)
        # error_out True表示页数不足或超过纵页数时会报错 返回404状态码，默认为True
        items = []
        for e in r.items:
            d = {}
            d.update(inst_date=e.inst_date.strftime(date_formatter))
            d.update(inst_name=e.inst_name)
            d.update(inst_key=e.inst_key)
            d.update(inst_from=e.inst_from)
            d.update(inst_to=e.inst_to)
            d.update(inst_status=e.inst_status)
            items.append(d)
        return r.total, items       # items's total number & the actual items content
    except Exception as e:
        print(e)
        print("<指令日志分页查询异常！>")