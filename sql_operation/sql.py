#!/usr/bin/env python3
# -*- encoding:utf-8 -*-
# Author:caoy
# @Time:  12:32

import os
import sys
import re
import time
import json
import pymysql
sys.path.append(r"D:\python_project\flask_57\flask_server\XMLParse")
# print(sys.path)
# print(os.getcwd())
# from parse_xml import ParseXML
from XMLParse.parse_xml import ParseXML



def parse_cfg(fold_path, file_name, endwith='.cfg'):
    """
    :parse the cfg file of sql's configure file
    :param fold_path:
    :param file_name:
    :param endwith:
    :return:
    """
    file = fold_path+'\\'+file_name+endwith
    if os.path.exists(file):
        try:
            with open(file, 'r', encoding='utf-8') as f_json:
                result = json.load(f_json)
                return result
        except Exception as e:
            print("sql cfg parse error!:{}".format(e))

    else:
        print(file+'is not exist!')


class Sql(object):
    def __init__(self, fold_path: str = r'./static/config/mysql/init', cfg_name: str='sql-init'):
        self.cfg = parse_cfg(fold_path, cfg_name)
        self.localhost = self.cfg['host']
        self.user = self.cfg['user']
        self.password = self.cfg['password']
        self.db_name = self.cfg['db_name']
        self.table_schema = self.cfg['schema']
        self.last_row_id = 0
        self.command = parse_cfg(r'./static/config/mysql/command', 'sql_command')

        self.deconding_xml_array = []    # search from sql and export xml's file(global)

        self.upload_xml_to_sql_array = []  # parse the upload xml file and insert into the table

    def __creat_database(self, db_name: str):
        pass

    def _connet(self):  # 连接MySQL数据库
        try:
            self.db = pymysql.connect(self.localhost, self.user, self.password, self.db_name)
            self.cursor = self.db.cursor()
        except Exception as e:
            print('<sql connected error!:{}>'.format(e))

    def _close(self):
        self.cursor.close()
        self.db.close()

    # def __edit(self, sql: str, data, type: str, many=False):
    #     res = None
    #     try:
    #         # self._connet()
    #         if many:
    #             res = self.cursor.executemany(sql, data)
    #         else:
    #             res = self.cursor.execute(sql, data)
    #         # 执行完插入或删除或修改操作后,需要调用一下conn.commit()方法进行提交.这样,数据才会真正保 存在数据库中
    #         if type == 'spectrum':
    #             self.last_row_id = self.cursor.last_row_id
    #         self.db.commit()
    #         # self._close()
    #     except Exception as e:
    #         print("<事务提交失败！,{}>".format(e))
    #         self.db.rollback()
    #     return res

    # def _insert(self, sql: str, data, table_name: str, many=False):
    #     return self.__edit(sql, data, table_name, many)

    def insert_decoding_data(self, cfg, obj: dict):
        res = None
        try:
            self._connet()
            self.cursor.executemany(cfg['insert_spectrum_info'], self.__parse_data(obj, self.last_row_id))
            self.last_row_id = self.cursor.lastrowid
            self.cursor.executemany(cfg['insert_sat_info'], self.__parse_data(obj, self.last_row_id, 'sat_info'))
            res = self.cursor.executemany(cfg['insert_dbs_info'], self.__parse_data(obj, self.last_row_id, 'dbs_info'))

            # 执行完插入或删除或修改操作后,需要调用一下conn.commit()方法进行提交.这样,数据才会真正保 存在数据库中

            self.db.commit()
            # self._close()
        except Exception as e:
            print("<事务提交失败！,{}>".format(e))
            self.db.rollback()
        finally:
            self._close()
        return res

    def __parse_data(self, obj: dict, current_row: int, types='spectrum_info'):
        """
        : used to dynamic parse xml's structure(obj) and insert into sql
        :param obj: is the parsed xml structrue
        :param current_row: 当前插入sql row num（key id）
        :return:
        """
        pattern_prefix = re.compile(r'\d+(\.\d+)?')     # THE DECIMAL VALUE
        pattern_suffix = re.compile(r'[A-Za-z]+')       # the E S W N PREFIX

        array = []
        if 'spectrum_info' == types:
            FORMATER = "%Y-%m-%d %H:%M:%S"
            array.append((time.strftime(FORMATER, time.localtime()),

                          obj['处理时间'],

                          self.__extract_data(pattern_prefix.search(obj['关口站']['经度']).group(),
                                              pattern_suffix.search(obj['关口站']['经度']).group()),

                          self.__extract_data(pattern_prefix.search(obj['关口站']['纬度']).group(),
                                              pattern_suffix.search(obj['关口站']['纬度']).group()),

                          obj['网络号'],

                          obj['上行频率基准kHz'],

                          obj['卫星编号'],

                          obj['接收频率'],

                          self.__extract_data(pattern_prefix.search(obj['设备位置']['经度']).group(),
                                              pattern_suffix.search(obj['设备位置']['经度']).group()),

                          self.__extract_data(pattern_prefix.search(obj['设备位置']['纬度']).group(),
                                              pattern_suffix.search(obj['设备位置']['纬度']).group())))

        elif 'sat_info' == types:
            for item in obj['卫星']:
                array.append((current_row,
                              obj['卫星'][item]['卫星编号'],
                              self.__extract_data(pattern_prefix.search(obj['卫星'][item]['经度']).group(),
                                                  pattern_suffix.search(obj['卫星'][item]['经度']).group()),

                              obj['卫星'][item]['信号频率']))

        else:
            for item in obj['点波束']:
                array.append((current_row,

                              obj['点波束'][item]['前向载波下行频率MHz'],

                              obj['点波束'][item]['前向载波速率kBd'],

                              obj['点波束'][item]['直径km'],

                              self.__extract_data(pattern_prefix.search(obj['点波束'][item]['中心纬度']).group(),
                                                  pattern_suffix.search(obj['点波束'][item]['中心纬度']).group()),

                              self.__extract_data(pattern_prefix.search(obj['点波束'][item]['中心经度']).group(),
                                                  pattern_suffix.search(obj['点波束'][item]['中心经度']).group()),

                              obj['点波束'][item]['点波束号'],

                              self.__dbs_attribute(obj['点波束'][item]['点波束属性'])))
        return array

    def __search_rows(self, cfg, table_name: str):
        try:
            self._connet()
            self.cursor.execute("use information_schema")
            self.cursor.execute(cfg["search_rows"].format(self.table_schema, table_name))
            result = self.cursor.fetchone()
            return result
        except Exception as e:
            print('<search all rows error!-{}>'.format(e))
            return None
        finally:
            self.cursor.execute("use" + " " + self.table_schema)
            self._close()


    def update_coding_log(self, cfg, code_id: str, status: int):
        """
        : update the status of the code id
        :param cfg:
        :param code_id:
        :param status:
        :return:
        """
        try:
            self._connet()
            res = self.cursor.executemany(cfg["insert_coding_log"].format(code_id, status))
            self.db.commit()
        except Exception as e:
            print('<insert coding log sql error!-{0}>'.format(e))
            self.db.rollback()
            res = None
        finally:
            self._close()
            return res

    def insert_coding_log(self, cfg, items: list):
        """
        :param cfg: the sql command
        :param items: the will inserting items [(),(),..]
        :return:
        """
        try:
            self._connet()
            res = self.cursor.executemany(cfg["insert_coding_log"], items)
            self.db.commit()

        except Exception as e:
            print('<insert coding log sql error!-{0}>'.format(e))
            res = None
            self.db.rollback()
        finally:
            self._close()
            return res

    def __search_coding_log_by_limit(self, cfg, page_number: int, page_size: int):
        """
        :single page search
        :param cfg: sql command
        :param page_number:
        :param page_size:
        :return: list
        """

        try:
            self._connet()
            res = self.cursor.execute(cfg['search_coding_log'].format((page_number-1)*page_size, page_size))
            # results = self.cursor.fetchall()[0][0].split(',')
            results = self.cursor.fetchall()
            return results, len(results)
        except Exception as e:
            print('<search coding log error!-{0}>'.format(e))
        finally:
            self._close()

    def search_coding_log(self, cfg, page_number: int, page_size: int, table_name: str = 'log_coding')->list:
        """

        :param cfg:
        :param page_number:
        :param page_size:
        :return:
        """
        FORMATER = "%Y-%m-%d %H:%M:%S"
        result = []
        try:
            items, _ = self.__search_coding_log_by_limit(cfg, page_number, page_size)
            rows = self.__search_rows(cfg, table_name)[0]
            for e in items:         # exception: items is a list
                item = dict()
                item["date"] = e[0].strftime(FORMATER)
                item["code_name"] = e[1]
                item["code_id"] = e[2]
                item["addr_from"] = e[3]
                item["addr_to"] = e[4]
                item["status"] = e[-1]          # 0：success   1：padding   2：failure
                item["rows"] = rows
                result.append(item)
        except Exception as e:
            print("<search coding log error!-{}>".format(e))
            result = None
        finally:
            return result


    def search(self, cfg, start_time='', stop_time=''):
        """
        :param cfg:  the json, sql's command
        :param start_time:
        :param stop_time:
        :return:
        """
        try:
            self._connet()
            keys, _ = self._search_keys_from_time(cfg, start_time, stop_time)  # 通过时间获取keys
            self.deconding_xml_array.clear()
            for key in keys:
                obj = {'卫星': {}, '点波束': {}}
                # res = self.cursor.execute(cfg['search_decoding_info'].format(key))
                res = self.cursor.execute(cfg['search_spectrum_info'].format(key))
                result = self.cursor.fetchall()
                self.__parse_spectrum_info(obj, result)   # parse the spectrum table's data ande create object
                res = self.cursor.execute(cfg['search_sat_info'].format(key))
                results = self.cursor.fetchall()
                for index, result in enumerate(results):
                    obj['卫星']['卫星'+str(index).zfill(len(str(len(results))))] = self.__parse_sat_info(result)

                res = self.cursor.execute(cfg['search_dbs_info'].format(key))
                results = self.cursor.fetchall()
                for index, result in enumerate(results):
                    obj['点波束']['点波束' + str(index).zfill(len(str(len(results))))] = self.__parse_dbs_info(result)

                self.deconding_xml_array.append(obj)
            # self.db.commit()
        except Exception as e:
            print('<search sql error!{}>'.format(e))
        finally:
            self._close()

    def _search_keys_from_time(self, cfg, start: str, stop: str):
        """
        :通过时间区间获取key id
        :param start:
        :param stop:
        :return: list of key's id
        """
        res = self.cursor.execute(cfg['search_key_from_time'].format(start, stop))
        results = self.cursor.fetchall()[0][0].split(',')
        return results, len(results)

    def __parse_spectrum_info(self, obj: dict, sql_result: tuple)->dict:
        """parse the sql's restlt to obj. the obj's structure
        equal the obj transfer to front endian"""
        res = sql_result[0]

        obj['处理时间'] = res[0].strftime('%Y%m%d%H%M%S')
        obj['关口站'] = {'纬度': self.__splicing_data(res[2], 'lat'),
                        "经度": self.__splicing_data(res[1], 'lon')}
        obj['网络号'] = str(res[3])
        obj["上行频率基准kHz"] = str(res[4])
        obj["卫星编号"] = res[5]
        obj["接收频率"] = str(res[6])
        obj["设备位置"] = {'纬度': self.__splicing_data(res[7], 'lat'),
                           "经度": self.__splicing_data(res[-1], 'lon')}

    def __parse_sat_info(self, sql_result: tuple)->dict:
        """parse the sql's result to object"""
        item = {}
        item["卫星编号"] = sql_result[0]
        item['经度'] = self.__splicing_data(sql_result[1], 'lon')
        item['信号频率'] = str(sql_result[-1])
        return item

    def __parse_dbs_info(self, sql_result: tuple)->dict:
        """"""
        item = {}
        item['前向载波下行频率MHz'] = str(sql_result[0])
        item['前向载波速率kBd'] = str(sql_result[1])
        item['直径km'] = str(sql_result[2])
        item['中心纬度'] = self.__splicing_data(sql_result[3], 'lat')
        item['中心经度'] = self.__splicing_data(sql_result[4], 'lon')
        item['点波束号'] = str(sql_result[5])
        item['点波束属性'] = str(sql_result[-1])
        return item

    def __endswith_sign(self, data: str, lon_lat: str):
        """East West South North 符号结尾"""
        if lon_lat.lower() in ['lon', 'longitude']:
            if float(data) == 0:
                return ''
            else:
                return '°E' if float(data) > 0 else '°W'
        elif lon_lat.lower() in ['lat', 'latitude']:
            if float(data) == 0:
                return ''
            else:
                return '°N' if float(data) > 0 else '°S'

    def __splicing_data(self, data: float, sign: str) ->str:
        """East West South North 符号结尾"""
        if sign.lower() in ['lon', 'longitude']:
            if float(data) == 0:
                return str(float(data))
            else:
                return str(abs(float(data))) + ('°E' if float(data) > 0 else '°W')
        elif sign.lower() in ['lat', 'latitude']:
            if float(data) == 0:
                return str(float(data))
            else:
                return str(abs(float(data))) + ('°N' if float(data) > 0 else '°S')


    def __extract_data(self, data: str, sign: str) ->str:
        """
        : extract data and charge it positive or negative by endswith sign
        :param data: the original data from re.search
        :param sign: the endswith sign used to charge negative( S W ) or positive( N E )
        :return:
        """
        if sign.upper() in ['S', 'W']:
            return '-' + data
        else:   # S W
            return data

    def __dbs_attribute(self, item: str)->int:
        """get the dbs attribute: 0 for local; 1 for neighbor; 2 for far"""
        if item == 'local_beam':
            return 0
        elif item == 'neighbor_beam':
            return 1
        else:
            return 2


if __name__ == '__main__':
    # config = parse_cfg(r'..\static\config\mysql\init', 'sql-init')
    message = parse_cfg(r'..\static\config\mysql\command', 'sql_command')
    # xml_file = r'C:\Users\Lenovo\Pictures\57\分析应用软件参考文档，已脱密\GSC_DDC2100k_signalling（2）.xml'
    xml_file = r'C:\Users\Lenovo\Pictures\57\分析应用软件参考文档，已脱密\verson2\5F1_01_19707000_20200325100509_0001.xml'
    xml = ParseXML(xml_file).xml_info
    FORMATER = "%Y-%m-%d %H:%M:%S"
    sql = Sql(r'..\static\config\mysql\init', 'sql-init')
    start = time.clock()

    # for i in range(5):
    #     sql.insert_decoding_data(message, xml)
    #     # sql.search()
    #     time.sleep(1)
    sql.search(message, '2020-04-14 19:19:11', '2020-04-14 19:19:17')
    end = time.clock()

    print(end-start)

    print('everthing is ok!')

