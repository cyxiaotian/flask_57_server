#!/usr/bin/env python3
# -*- encoding:utf-8 -*-
# Author:caoy
# @Time:  16:22

from queue import Queue as TQueue
from multiprocessing import Queue as MQueue

CODE_T_QUEUE = TQueue(5)         # multi threading code queue

CODE_M_QUEUE = MQueue(5)         # multi processing code queue

PROFILE_CONFIG_M_QUEUE = MQueue(5)  # profile configure setting queue (multi processing queue)


LOCAL_XML_URL = r'http://127.0.0.1:5000/static/zip_file/xx'